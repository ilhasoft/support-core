# Ilhasoft Support Core CHANGELOG

## Version 0.8.8 (2017-05-14)

* [Fix BitmapCompressor class](https://bitbucket.org/ilhasoft/support-core/issues/8/compross-o-de-imagem-com-problema)
* Change for don't use a deprecated method of `rx.Observable`
* Implemented new item decoration to specify space on all sides of an item
* Update CHANGELOG

## Version 0.8.7 (2017-04-16)

* Fix manifest of databinding library that was replacing application
* Update android mask library to version without application tag on Manifest

## Version 0.8.6 (2017-04-13)

* Add create option for `BadgeOptionMenuDrawable` with custom padding

## Version 0.8.5 (2017-04-11)

* Refactor file compressor class to support multiple kinds of compression

## Version 0.8.4 (2017-03-27)

* Fix get mime type from uri method
* Fix method visibility on `IoHelper`
* Add flag for multiple or single file on media selector adapter
* Add mime type retrieval from Uri

## Version 0.8.1 (2017-02-09)

* Add exif interceptor to avoid rotated pictures

## Version 0.8.0 (2017-01-24)

### Changes

* Division of the library into several sub modules
* Small visual adjust on `BadgeDrawable`
* Improved BadgeDrawable API

### Bug fixes

* Fix padding problem on `BadgeDrawable` for pre-lollipop devices

## Version 0.7.8 (2017-01-13)

* Add creator method with common icon
* Add badge drawable for menu item

## Version 0.7.6 (2017-01-05)

* Remove preferences when value null is configured

## Version 0.7.5 (2017-01-04)

* Fix replace label issue
* Add binding mask on text view

## Version 0.7.3 (2016-12-21)

* Refactor Picasso image creator with centerCrop
* Add image binding option with centerCrop option

## Version 0.7.1 (2016-12-19)

* Fixed bug on `RecyclerAdapterWrapper` and consequently in `HeaderRecyclerAdapter`

## Version 0.7.0 (2016-12-16)

* Add overloaded method to add object without adapter notification
* Add clear method without notifyDataSetChanged

## Version 0.6.8 (2016-12-13)

* Update video compressor library to fix errors on videos without metadata

## Version 0.6.7 (2016-12-12)

* Add isolated method to clear filtered objects
* Add ids for default view in wrapper adapters

## Version 0.6.6 (2016-12-06)

* Add intent creator on `FileHelper`
* Add default click actions for media view models

## Version 0.6.3 (2016-12-02)

* Add support for get multiple files on media selector
* Add audio recorder on media selector delegate
* Add options to customize media selector

## Version 0.6.2 (2016-12-01)

* Fix position issue on binding view holder
* Add FooterRecyclerAdapter

## Version 0.6.0 (2016-11-23)

### Changes

* Add permission to read external storage
* Updated support libraries and build tools and compiled sdk version
* Update MaterialProgressBar library
* Adjusts on OnDemand implementations
* Added depreciation message to `DividerItemDecoration`
* [Now the listeners extend from Serializable. Added option to customize open media icon and does not show it](https://bitbucket.org/ilhasoft/support-core/issues/4/it-was-too-bad-the-api-to-pass-listeners)
* Created `HtmlHelper` class
* Add method on `ColorHelper` to get color hex by resource
* Add file compressor library
* Add java 8 support with retrolambda

### Bug fixes

* [HeaderRecyclerAdapter It should be increasing the total of items](https://bitbucket.org/ilhasoft/support-core/issues/5/headerrecycleradapter-it-should-be)
* [RecyclerAdapterWrapper does not emit changes to the adapter it wraps](https://bitbucket.org/ilhasoft/support-core/issues/6/recycleradapterwrapper-does-not-emit)
* [RecyclerAdapterWrapper attach RecyclerView parent bug](https://bitbucket.org/ilhasoft/support-core/issues/7/recycleradapterwrapper-attach-recyclerview)
* Fixed **ids.xml** location

## Version 0.5.9 (2016-10-11)

* Add media selector for image or video

## Version 0.5.8 (2016-10-07)

* Add support to change video duration on media selector class

## Version 0.5.7 (2016-10-06)

* Update `ParallaxPageTransformer` to support specialization on applying parallax effect
* Update `ParallaxTransformInformation` to support specialization

## Version 0.5.5 (2016-09-23)

* Request missing permissions when getting image from gallery
* Update code to support Android Nougat changes on taking pictures

## Version 0.5.3 (2016-09-19)

* Added parallax page transformer for `ViewPagers`

## Version 0.5.2 (2016-09-18)

* Refactor `TopCropImageview` for Android 5- 

## Version 0.5.1 (2016-09-15)

* Remove binding for font bevause of performance issues

## Version 0.5.0 (2016-08-26)

* Updated media view to support a new set of customizations
    * Auto close when use default activity;
    * Enable or disable title;
    * Custom title color;
    * Custom toolbar color;
    * Custom view pager background color;
    * Custom back button color;
    * Custom close button;
* Fixed media view image loading;
* Changed media view listeners;

## Version 0.4.2 (2016-06-16)

* Fixed toolbar of media view module;
* Fixed small error on FragmentTransactionHelper;
* Make AutoRecyclerAdapter more flexible and portable;

## Version 0.4.0 (2016-06-14)

* Refactor on MediaSelector, now this is more simple and a efficient, using BottonSheet and Renamed to `MediaSelectorDelegate`;
* Updated the `EditTextDialog` to be more flexible;
* Update Media classes to use the `EditTextDialog`;
* Added MediaView feature;
* Added helper classes for TextViews and actionBars;
* Improvements on `FilterableRecyclerAdapter`;
* Removed `EmptyRecyclerAdapter` and implemented this on `ListRecyclerAdapter`;
* Implemented `AutoRecyclerAdapter` to improve development speed;
* Adjusts in on demand adapters;
* Update sample structure;
* Added MediaView to sample;
* Added `AutoRecyclerAdapter` to sample;
* Small adjusts on MultiRowsRadioGroup and TopCropImage;
* Renamend TopCropImage to TopCropImageView;

## Version 0.3.1 (2016-05-30)

* Added binding for TextViews;

## Version 0.3.0 (2016-05-29)

* Added bindings for some widgets;

## Version 0.2.0 (2016-05-25)

* Added useful classes to work with graphics and bitmaps (package graphics);
* Adjust on base preference class;
* Added:
    * `DatePickerFragment`;
    * `FadePageTransformer` (for ViewPagers);
    * Helpers to use with resources and context;
* Widgets
    * `Pacman view`;
    * `Ring view`;
    * `TopCropImage`;
* Implemented classes to help you select several different types of medias, including YouTube media;
* Implemented sample project;

## Version 0.1.0 (2016-05-23)

* **Initial release**;
* Added animatior for power save mode;
* Created:
    * Helper class to get custom elapsed times;
    * Helper for fragment transactions;
    * Helper to create question dialog;
    * EditTextDialog;
    * IndeterminateProgressDialog with material design;
    * Helper for Bitmaps;
    * Some useful bitmap transformations;
    * Helpers for common tasks on **helpers** package;
    * Util listeners;
    * A basic preference class;
    * A **Span** for **Typeface**;
* Basic implementation of a MVP library;
* Implemented a RecyclerView.ViewHolder with util methods;
* Impelemented a SlideScrollAwareBehavior to FloatingActionButtons;
* Widgets:
    * `ContentePager`;
    * `MultiRowsRadioGroup`;
    * `SimpleCircularBitmapImageView`;
    * `TouchImageView`;
    * `UntouchableRecyclerView`;
    * `WrapContentViewPager`;
* Adapters:
    * `EmptyRecyclerAdapter`;
    * `ListRecyclerAdapter`;
    * `FilterableRecyclerAdapter`;
    * `OnDemandAdapter`;
    * `OnDemandWrapperAdapter`;
    * `HeaderRecyclerAdapter`;
    * `RecyclerAdapterWrapper`;
    * `WidgetPagerAdapter`;