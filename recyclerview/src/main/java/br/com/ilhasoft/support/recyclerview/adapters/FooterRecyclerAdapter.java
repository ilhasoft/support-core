package br.com.ilhasoft.support.recyclerview.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by daniel on 02/03/16.
 */
public class FooterRecyclerAdapter extends RecyclerAdapterWrapper {

    private static final int TYPE_FOOTER = -1;

    private final View viewFooter;

    public FooterRecyclerAdapter(View viewFooter, RecyclerView.Adapter wrappedAdapter) {
        super(wrappedAdapter);
        this.viewFooter = viewFooter;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        final int lastPosition = getItemCount() - 1;
        if (position >= lastPosition) {
            return TYPE_FOOTER;
        } else {
            return super.getItemViewType(position);
        }
    }

    @Override
    public long getItemId(int position) {
        if (getItemViewType(position) == TYPE_FOOTER) {
            return Long.MAX_VALUE;
        } else {
            return super.getItemId(position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                return new RecyclerView.ViewHolder(viewFooter) { };
            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) != TYPE_FOOTER) super.onBindViewHolder(holder, position);
    }

}
