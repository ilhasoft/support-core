package br.com.ilhasoft.support.recyclerview.decorations;

import android.graphics.Rect;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by johncordeiro on 7/15/15.
 */
public class LinearSpaceItemDecoration extends BaseItemDecoration {

    private int space;

    public LinearSpaceItemDecoration(@Orientation int orientation) {
        super(orientation);
    }

    public LinearSpaceItemDecoration(@Orientation int orientation, int space) {
        super(orientation);
        this.space = space;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final int adapterPosition = parent.getChildAdapterPosition(view);
        if (orientation == LinearLayoutManager.VERTICAL) {
            getVerticalItemOffsets(outRect, adapterPosition);
        } else {
            getHorizontalItemOffsets(outRect, adapterPosition);
        }
    }

    private void getVerticalItemOffsets(Rect outRect, int adapterPosition) {
        outRect.bottom = space;
        if (adapterPosition == 0) {
            outRect.top = space;
        }
    }

    private void getHorizontalItemOffsets(Rect outRect, int adapterPosition) {
        outRect.right = space;
        if (adapterPosition == 0) {
            outRect.left = space;
        }
    }

}
