package br.com.ilhasoft.support.recyclerview.decorations;

import android.graphics.Rect;
import androidx.annotation.Px;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Now this is a simpler but more generic implementation that supports defining a space on all
 * sides of an item.
 *
 * To use the above implementation use the new {@link LinearSpaceItemDecoration} class.
 *
 * Created by danielsan on 5/14/17.
 */
public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    @Px
    private int top;
    @Px
    private int left;
    @Px
    private int right;
    @Px
    private int bottom;

    public SpaceItemDecoration() { }

    public SpaceItemDecoration(@Px int left, @Px int top, @Px int right, @Px int bottom) {
        setSpace(left, top, right, bottom);
    }

    public void setSpace(@Px int left, @Px int top, @Px int right, @Px int bottom) {
        this.top = top;
        this.left = left;
        this.right = right;
        this.bottom = bottom;
    }

    @Px
    public int getTop() {
        return top;
    }

    public void setTop(@Px int top) {
        this.top = top;
    }

    @Px
    public int getLeft() {
        return left;
    }

    public void setLeft(@Px int left) {
        this.left = left;
    }

    @Px
    public int getRight() {
        return right;
    }

    public void setRight(@Px int right) {
        this.right = right;
    }

    @Px
    public int getBottom() {
        return bottom;
    }

    public void setBottom(@Px int bottom) {
        this.bottom = bottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.top = top;
        outRect.left = left;
        outRect.right = right;
        outRect.bottom = bottom;
    }

}
