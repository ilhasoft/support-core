package br.com.ilhasoft.support.recyclerview.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by daniel on 02/03/16.
 */
public class HeaderRecyclerAdapter extends RecyclerAdapterWrapper {

    private static final int TYPE_HEADER = -1;

    private final View viewHeader;

    public HeaderRecyclerAdapter(View viewHeader, RecyclerView.Adapter wrappedAdapter) {
        super(wrappedAdapter);
        this.viewHeader = viewHeader;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return super.getItemViewType(position - 1);
        }
    }

    @Override
    public long getItemId(int position) {
        if (getItemViewType(position) == TYPE_HEADER) {
            return Long.MAX_VALUE;
        } else {
            return super.getItemId(position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new HeaderViewHolder(viewHeader);
            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position > 0) super.onBindViewHolder(holder, position - 1);
    }

    @Override
    protected void notifyItemRangeChangedWrapper(int positionStart, int itemCount) {
        super.notifyItemRangeChangedWrapper(positionStart + 1, itemCount);
    }

    @Override
    protected void notifyItemRangeChangedWrapper(int positionStart, int itemCount, Object payload) {
        super.notifyItemRangeChangedWrapper(positionStart + 1, itemCount, payload);
    }

    @Override
    protected void notifyItemRangeInsertedWrapper(int positionStart, int itemCount) {
        super.notifyItemRangeInsertedWrapper(positionStart + 1, itemCount);
    }

    @Override
    protected void notifyItemRangeRemovedWrapper(int positionStart, int itemCount) {
        super.notifyItemRangeRemovedWrapper(positionStart + 1, itemCount);
    }

    @Override
    protected void notifyItemRangeChangedWrapper(int fromPosition, int toPosition, int itemCount) {
        super.notifyItemRangeChangedWrapper(fromPosition + 1, toPosition, itemCount);
    }

    private static final class HeaderViewHolder extends RecyclerView.ViewHolder {
        HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

}
