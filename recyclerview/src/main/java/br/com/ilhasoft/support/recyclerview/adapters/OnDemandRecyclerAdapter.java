package br.com.ilhasoft.support.recyclerview.adapters;

import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collection;
import java.util.List;

/**
 * Created by daniel on 30/03/16.
 */
public abstract class OnDemandRecyclerAdapter<E, VH extends RecyclerView.ViewHolder>
        extends ListRecyclerAdapter<E, VH> {

    private boolean enableOnDemand = true;
    private OnDemandListener onDemandListener = null;

    public OnDemandRecyclerAdapter() {
        super();
    }

    public OnDemandRecyclerAdapter(OnDemandListener onDemandListener) {
        super();
        this.onDemandListener = onDemandListener;
    }

    public OnDemandRecyclerAdapter(int capacity) {
        super(capacity);
    }

    public OnDemandRecyclerAdapter(List<E> list) {
        super(list);
    }

    public OnDemandRecyclerAdapter(Collection<? extends E> collection) {
        super(collection);
    }

    @Override
    public void onBindViewHolder(VH holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (enableOnDemand && onDemandListener != null && position == getItemCount() - 1) {
            new Handler().postDelayed(runnableOnLoadMore, 200);
        }
    }

    @Nullable
    public OnDemandListener getOnDemandListener() {
        return onDemandListener;
    }

    public void setOnDemandListener(@Nullable OnDemandListener onDemandListener) {
        this.onDemandListener = onDemandListener;
    }

    public boolean isEnableOnDemand() {
        return enableOnDemand;
    }

    public void setEnableOnDemand(boolean enableOnDemand) {
        this.enableOnDemand = enableOnDemand;
    }

    private final Runnable runnableOnLoadMore = new Runnable() {
        @Override
        public void run() {
            onDemandListener.onLoadMore();
        }
    };

}
