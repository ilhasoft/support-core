package br.com.ilhasoft.support.recyclerview.adapters;

import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Created by daniel on 30/03/16.
 */
public class OnDemandWrapperAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerAdapterWrapper<VH> {

    private boolean enableOnDemand = true;
    private OnDemandListener onDemandListener;

    public OnDemandWrapperAdapter(RecyclerView.Adapter<VH> adapter) {
        this(adapter, null);
    }

    public OnDemandWrapperAdapter(RecyclerView.Adapter<VH> adapter,
                                  @Nullable OnDemandListener onDemandListener) {
        super(adapter);
        this.onDemandListener = onDemandListener;
    }

    @Override
    public void onBindViewHolder(VH holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (enableOnDemand && onDemandListener != null && position == getItemCount() - 1) {
            new Handler().postDelayed(runnableOnLoadMore, 200);
        }
    }

    @Nullable
    public OnDemandListener getOnDemandListener() {
        return onDemandListener;
    }

    public void setOnDemandListener(@Nullable OnDemandListener onDemandListener) {
        this.onDemandListener = onDemandListener;
    }

    public boolean isEnableOnDemand() {
        return enableOnDemand;
    }

    public void setEnableOnDemand(boolean enableOnDemand) {
        this.enableOnDemand = enableOnDemand;
    }

    private final Runnable runnableOnLoadMore = new Runnable() {
        @Override
        public void run() {
            onDemandListener.onLoadMore();
        }
    };

}
