package br.com.ilhasoft.support.core.sample.features.autoadapter;

import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import br.com.ilhasoft.support.recyclerview.adapters.ViewHolder;

/**
 * Created by daniel on 13/06/16.
 */
public class ItemViewHolder extends ViewHolder<Item> {

    private final TextView textView;

    public ItemViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView;
    }

    @Override
    protected void onBind(Item item) {
        textView.setText(String.format(Locale.getDefault(), "%d - %s", this.getAdapterPosition(), item.getText()));
    }

}
