package br.com.ilhasoft.support.core.sample.features.headeradapter;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Locale;
import java.util.Random;

import br.com.ilhasoft.support.core.sample.R;
import br.com.ilhasoft.support.core.sample.databinding.ActivityHeaderAdapterBinding;
import br.com.ilhasoft.support.recyclerview.adapters.AutoRecyclerAdapter;
import br.com.ilhasoft.support.recyclerview.adapters.HeaderRecyclerAdapter;
import br.com.ilhasoft.support.recyclerview.adapters.OnCreateViewHolder;
import br.com.ilhasoft.support.recyclerview.adapters.ViewHolder;

/**
 * Created by danielsan on 19/12/16.
 */
public class HeaderAdapterActivity extends AppCompatActivity
        implements OnCreateViewHolder<HeaderAdapterActivity.Item, HeaderAdapterActivity.ItemViewHolder> {

    private ActivityHeaderAdapterBinding binding;
    private AutoRecyclerAdapter<Item, ItemViewHolder> adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_header_adapter);
        adapter = new AutoRecyclerAdapter<>(this);
        binding.list.setAdapter(new HeaderRecyclerAdapter(createHeaderView(), adapter));
        binding.setActivity(this);
    }

    public void onClickAddItem() {
        adapter.add(new Item(nextRandomString()));
    }

    public void onClickRemoveItem() {
        if (adapter.isEmpty()) return;

        final int index = new Random().nextInt(adapter.getItemCount());
        adapter.remove(index);
        Toast.makeText(this, String.format(Locale.US, "Removed: %d", index + 1), Toast.LENGTH_SHORT).show();
    }

    private View createHeaderView() {
        final AppCompatTextView textView = new AppCompatTextView(this);
        textView.setText("I'm the header view!!!");
        return textView;
    }

    private String nextRandomString() {
        final CharSequence characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        final StringBuilder string = new StringBuilder();
        Random random = new Random();
        while (string.length() < 8) {
            string.append(characters.charAt(random.nextInt(characters.length())));
        }
        return string.toString();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(LayoutInflater layoutInflater, ViewGroup parent,
                                             int viewType) {
        return new ItemViewHolder(new AppCompatTextView(this));
    }

    public static final class ItemViewHolder extends ViewHolder<Item> {
        ItemViewHolder(AppCompatTextView itemView) {
            super(itemView);
        }
        @Override
        protected void onBind(Item item) {
            ((AppCompatTextView) itemView).setText(item.text);
        }
    }

    public static final class Item {
        public final String text;
        private Item(String text) {
            this.text = text;
        }
    }

}
