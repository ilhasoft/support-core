package br.com.ilhasoft.support.core.sample.models;

/**
 * Created by john-mac on 6/11/16.
 */
public class ItemSample {

    public String name;

    public Class mClass;

    public ItemSample(String name, Class mClass) {
        this.name = name;
        this.mClass = mClass;
    }

    @Override
    public String toString() {
        return name;
    }
}
