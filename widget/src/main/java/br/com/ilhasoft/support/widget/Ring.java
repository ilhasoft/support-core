package br.com.ilhasoft.support.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class Ring extends View {

    private static final String TAG = "Ring";

    private Paint paint;
    private Paint paintBackground;
    private RectF rect;

    private int progress = 0;
    private float angle;

    private float strokeWidth = 7;
    private int strokeColor = Color.BLACK;
    private int strokeBackgroundColor = Color.TRANSPARENT;

    public Ring(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Ring, 0, 0);
        try {
            strokeColor = typedArray.getColor(R.styleable.Ring_ringStrokeColor, Color.BLACK);
            strokeBackgroundColor = typedArray.getColor(R.styleable.Ring_ringStrokeBackgroundColor, Color.TRANSPARENT);
            strokeWidth = typedArray.getDimension(R.styleable.Ring_ringStrokeWidth, strokeWidth);
            progress = typedArray.getInteger(R.styleable.Ring_ringProgress, progress);
            setProgress(progress);
        } finally {
            typedArray.recycle();
        }

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(strokeColor);

        paintBackground = new Paint();
        paintBackground.setAntiAlias(true);
        paintBackground.setStyle(Paint.Style.STROKE);
        paintBackground.setStrokeWidth(strokeWidth);
        paintBackground.setColor(strokeBackgroundColor);

        rect = new RectF(0, 0, 0, 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        float offset = strokeWidth + 2;
        rect.set(offset, offset, getMeasuredWidth() - offset, getMeasuredHeight() - offset);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawArc(rect, -90, getAngleByProgress(100), false, paintBackground);
        canvas.drawArc(rect, -90, angle, false, paint);
    }

    public int getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(String strokeColor) {
        this.strokeColor = Color.parseColor(strokeColor);
        paint.setColor(this.strokeColor);
    }

    public void setStrokeColor(int color){
        paint.setColor(color);
        invalidate();
    }

    public void setStrokeWidth(int width){
        paint.setStrokeWidth(width);
        invalidate();
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
        this.angle = getAngleByProgress(progress);
    }

    private float getAngleByProgress(int progress) {
        return -3.6f * progress;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

}
