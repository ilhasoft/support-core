package br.com.ilhasoft.support.widget.behaviors;

import android.content.Context;
import android.os.Build;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

import java.util.List;

import br.com.ilhasoft.support.widget.R;

/**
 * Created by daniel on 05/07/16.
 */

public class GenericFabBehaviour<V extends View> extends CoordinatorLayout.Behavior<V> {

    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
    private static final boolean SNACK_BAR_BEHAVIOR_ENABLED = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;

    private Context context;
    private float translationY;
    private boolean isAnimatingOut;

    public GenericFabBehaviour(Context context) {
        this.context = context;
    }

    public GenericFabBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, V child, View dependency) {
        return SNACK_BAR_BEHAVIOR_ENABLED && dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, V child, View dependency) {
        if (dependency instanceof Snackbar.SnackbarLayout) {
            final float translationY = getTranslationYForSnackBar(parent, child);
            if (this.translationY != translationY) {
                this.translationY = translationY;
                child.setTranslationY(translationY);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, V child,
                                       View directTargetChild, View target, int nestedScrollAxes) {
        return (nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL)
                && (Math.round(translationY) == 0);
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, V child, View target,
                               int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        if (dyConsumed > 0 && !isAnimatingOut && child.getVisibility() == View.VISIBLE) {
            // User scrolled down and the FAB is currently visible -> hide the FAB
            animateOut(child);
        } else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
            // User scrolled up and the FAB is currently not visible -> show the FAB
            animateIn(child);
        }
    }

    protected void animateOut(final View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            float verticalTranslation = view.getMeasuredHeight()
                    + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32f, context.getResources().getDisplayMetrics());
            ViewCompat.animate(view).translationY(verticalTranslation)
                    .setInterpolator(INTERPOLATOR).withLayer()
                    .setListener(new ViewPropertyAnimatorListener() {
                        public void onAnimationStart(View view) {
                            isAnimatingOut = true;
                        }
                        public void onAnimationCancel(View view) {
                            isAnimatingOut = false;
                        }
                        public void onAnimationEnd(View view) {
                            isAnimatingOut = false;
                            view.setVisibility(View.GONE);
                        }
                    }).start();
        } else {
            Animation anim = AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_to_down);
            anim.setInterpolator(INTERPOLATOR);
            anim.setDuration(300L);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    isAnimatingOut = true;
                }
                public void onAnimationEnd(Animation animation) {
                    isAnimatingOut = false;
                    view.setVisibility(View.GONE);
                }
                @Override
                public void onAnimationRepeat(final Animation animation) { }
            });
            view.startAnimation(anim);
        }
    }

    protected void animateIn(View view) {
        view.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            ViewCompat.animate(view).translationY(0).setInterpolator(INTERPOLATOR)
                    .withLayer().setListener(null).start();
        } else {
            Animation anim = AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_from_down);
            anim.setDuration(300L);
            anim.setInterpolator(INTERPOLATOR);
            view.startAnimation(anim);
        }
    }

    private float getTranslationYForSnackBar(CoordinatorLayout parent, V view) {
        float minOffset = 0;
        final List<View> dependencies = parent.getDependencies(view);
        for (int i = 0, z = dependencies.size(); i < z; i++) {
            final View dependence = dependencies.get(i);
            if (dependence instanceof Snackbar.SnackbarLayout && parent.doViewsOverlap(view, dependence)) {
                minOffset = Math.min(minOffset,
                        ViewCompat.getTranslationY(dependence) - dependence.getHeight());
            }
        }

        return minOffset;
    }

}
