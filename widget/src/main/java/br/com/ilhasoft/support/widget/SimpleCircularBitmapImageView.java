package br.com.ilhasoft.support.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by danielsan on 11/9/15.
 */
public class SimpleCircularBitmapImageView extends AppCompatImageView {

    private Bitmap circleBitmap;
    private int lastDiameter = -1;
    private Bitmap lastBitmap = null;
    private CircleBitmapTransform circleBitmapTransform;

    public SimpleCircularBitmapImageView(Context context) {
        super(context);
        circleBitmapTransform = new CircleBitmapTransform(lastDiameter, false);
    }

    public SimpleCircularBitmapImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        circleBitmapTransform = new CircleBitmapTransform(lastDiameter, false);
    }

    public SimpleCircularBitmapImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        circleBitmapTransform = new CircleBitmapTransform(lastDiameter, false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final Drawable drawable = getDrawable();
        final int diameter = Math.min(getWidth(), getHeight());
        if (diameter == 0 || drawable == null || !(drawable instanceof BitmapDrawable)) {
            super.onDraw(canvas);
        } else {
            final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            if (bitmap == null) {
                super.onDraw(canvas);
            } else {
                if (!bitmap.equals(lastBitmap) || lastDiameter != diameter) {
                    lastBitmap = bitmap;
                    lastDiameter = diameter;
                    circleBitmapTransform.setDiameter(diameter);
                    circleBitmap = circleBitmapTransform.transform(bitmap);
                }
                canvas.drawBitmap(circleBitmap, 0, 0, null);
            }
        }
    }

    private final class CircleBitmapTransform {
        private int diameter;
        private boolean canRecycleSource;
        public CircleBitmapTransform(final int diameter) {
            this(diameter, true);
        }
        public CircleBitmapTransform(final int diameter, final boolean canRecycleSource) {
            this.diameter = diameter;
            this.canRecycleSource = canRecycleSource;
        }
        public Bitmap transform(Bitmap source) {
            final Bitmap outputTmp;
            boolean mustRecycleOutputTmp = source.getWidth() != diameter || source.getHeight() != diameter;
            if (mustRecycleOutputTmp) {
                outputTmp = Bitmap.createScaledBitmap(source, diameter, diameter, false);
            } else {
                outputTmp = source;
            }

            final Bitmap output = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(output);

            final float radius = diameter / 2.0f;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, diameter, diameter);

            paint.setDither(true);
            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.parseColor("#BAB399")); // yellow
            canvas.drawCircle(radius, radius, radius, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(outputTmp, rect, rect, paint);

            if (mustRecycleOutputTmp) {
                outputTmp.recycle();
            }
            if (canRecycleSource) {
                source.recycle();
            }

            return output;
        }
        public void setDiameter(int diameter) {
            this.diameter = diameter;
        }
        public void setCanRecycleSource(boolean canRecycleSource) {
            this.canRecycleSource = canRecycleSource;
        }
    }

}
