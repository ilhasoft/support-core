package br.com.ilhasoft.support.media.view;

import java.io.Serializable;

/**
 * Created by daniel on 25/08/16.
 */
public interface OnClickCloseListener extends Serializable {

    void onClick(MediaViewFragment mediaViewFragment);

}
