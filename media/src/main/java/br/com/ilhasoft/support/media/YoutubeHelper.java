package br.com.ilhasoft.support.media;

import android.content.Context;
import androidx.annotation.StringDef;
import android.text.TextUtils;
import android.widget.Toast;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.ilhasoft.support.core.app.EditTextDialog;

/**
 * Created by danielsan on 10/06/16.
 */
public final class YoutubeHelper {

    private static final String THUMBNAIL_BASE_URL = "http://img.youtube.com/vi/%1$s/%2$s.jpg";

    public static final String URL_PATTERN = ".*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|e\\/|watch\\?v=|watch\\?.*v=)([^#&\\?]*).*";

    public static final String THUMBNAIL_DEFAULT = "default";
    public static final String THUMBNAIL_HIGH = "hqdefault";
    public static final String THUMBNAIL_MEDIUM = "mqdefault";
    public static final String THUMBNAIL_STANDARD = "sddefault";
    public static final String THUMBNAIL_MAXIMUM_RESOLUTION = "maxresdefault";

    @StringDef({THUMBNAIL_DEFAULT, THUMBNAIL_HIGH, THUMBNAIL_MEDIUM,
                THUMBNAIL_STANDARD, THUMBNAIL_MAXIMUM_RESOLUTION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ThumbnailQuality { }

    private YoutubeHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static void pickVideoFromInput(final Context context,
                                          final OnPickYoutubeVideoListener pickYoutubeVideoListener) {
        EditTextDialog.show(context, R.string.title_youtube_url, R.string.hint_youtube_url,
                android.R.string.ok, android.R.string.cancel, false, new EditTextDialog.OnInputTextListener() {
                    @Override
                    public void onInputText(EditTextDialog dialog, int which, CharSequence text) {
                        final String videoId = YoutubeHelper.getVideoIdFromUrl(text.toString());
                        if (TextUtils.getTrimmedLength(text) > 0 && videoId != null) {
                            pickYoutubeVideoListener.onPickYoutubeVideo(videoId, text.toString());
                            dialog.dismiss();
                        } else {
                            Toast.makeText(context, R.string.error_empty_youtube_url, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public static String getVideoUrlFromId(String youtubeId) {
        return "https://www.youtube.com/watch?v=" + youtubeId;
    }

    public static String getVideoIdFromUrl(String videoUrl) {
        final Matcher matcher = Pattern.compile(URL_PATTERN, Pattern.CASE_INSENSITIVE).matcher(videoUrl);
        return matcher.matches() ? matcher.group(1) : "";
    }

    public static String getThumbnailUrlFromUrl(String videoUrl, @ThumbnailQuality String thumbnailQuality) {
        return YoutubeHelper.getThumbnailUrlFromId(YoutubeHelper.getVideoIdFromUrl(videoUrl), thumbnailQuality);
    }

    public static String getThumbnailUrlFromId(String videoId, @ThumbnailQuality String thumbnailQuality) {
        return String.format(THUMBNAIL_BASE_URL, videoId, thumbnailQuality);
    }

    public interface OnPickYoutubeVideoListener {
        void onPickYoutubeVideo(String videoId, String videoUrl);
    }

}
