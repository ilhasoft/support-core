package br.com.ilhasoft.support.media.view;

import android.content.res.TypedArray;
import android.os.Bundle;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.ilhasoft.support.media.R;
import br.com.ilhasoft.support.core.helpers.ActionBarHelper;
import br.com.ilhasoft.support.core.helpers.BundleHelper;

/**
 * Show a list of {@link MediaModel} with a beautiful look and feel. This have a toolbar with
 * back button, a customizable title and a open action menu which can be visible or not.
 * <p>
 * The Medias will be shown in a way that can be passed forward and backward. By changing each
 * media the title will be changed as the media that appear, and the option to open it anger
 * appear or disappear.
 *
 * Created by johncordeiro on 04/09/15.
 */
public final class MediaViewFragment extends Fragment {

    static final String ARG_MEDIAS = "medias";
    static final String ARG_POSITION = "position";
    static final String ARG_TITLE_ENABLED = "title_enabled";
    static final String ARG_ON_CLICK = "on_click";
    static final String ARG_ON_VIEW_FINISHED = "on_view_finished";
    static final String ARG_ON_CLICK_CLOSE = "on_click_close";
    static final String ARG_DEFAULT_CLICK_DISABLED = "default_click_disabled";
    static final String ARG_TITLE_COLOR = "title_color";
    static final String ARG_TITLE_COLOR_RES = "title_color_res";
    static final String ARG_TOOLBAR_COLOR = "toolbar_color";
    static final String ARG_TOOLBAR_COLOR_RES = "toolbar_color_res";
    static final String ARG_BACKGROUND = "background";
    static final String ARG_BACKGROUND_RES = "background_res";
    static final String ARG_BACK_BUTTON_COLOR_RES = "back_button_color_res";
    static final String ARG_BACK_BUTTON_COLOR = "back_button_color";
    static final String ARG_CLOSE_BUTTON_ICON = "close_button_icon";

    private static final int NO_RESOURCE = Integer.MIN_VALUE;

    private int position;
    private MenuItem mnOpen;
    @ColorInt
    private int titleColor;
    @ColorInt
    private int toolbarColor;
    @ColorInt
    private int backgroundColor;
    @ColorInt
    private int backButtonColor;
    @DrawableRes
    private int closeButtonIcon;
    private boolean titleEnabled;
    private boolean defaultClickDisabled;
    private List<MediaModel> mediaList;
    @Nullable
    private OnClickCloseListener onClickCloseListener;
    @Nullable
    private OnClickMediaListener onClickMediaListener;
    @Nullable
    private OnViewFinishedListener onViewFinishedListener;
    private ViewPager medias;
    private Toolbar toolbar;

    static MediaViewFragment newInstance(Bundle args) {
        final MediaViewFragment fragment = new MediaViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        final Bundle arguments = BundleHelper.ensureArguments(this, ARG_MEDIAS, ARG_POSITION);
        mediaList = arguments.getParcelableArrayList(ARG_MEDIAS);
        position = arguments.getInt(ARG_POSITION, 0);
        titleColor = arguments.getInt(ARG_TITLE_COLOR, NO_RESOURCE);
        titleEnabled = arguments.getBoolean(ARG_TITLE_ENABLED, true);
        defaultClickDisabled = arguments.getBoolean(ARG_DEFAULT_CLICK_DISABLED, false);
        toolbarColor = arguments.getInt(ARG_TOOLBAR_COLOR, NO_RESOURCE);
        backgroundColor = arguments.getInt(ARG_BACKGROUND, NO_RESOURCE);
        backButtonColor = arguments.getInt(ARG_BACK_BUTTON_COLOR, NO_RESOURCE);
        closeButtonIcon = arguments.getInt(ARG_CLOSE_BUTTON_ICON, NO_RESOURCE);
        onClickMediaListener = (OnClickMediaListener) arguments.getSerializable(ARG_ON_CLICK);
        onClickCloseListener = (OnClickCloseListener) arguments.getSerializable(ARG_ON_CLICK_CLOSE);
        onViewFinishedListener = (OnViewFinishedListener) arguments.getSerializable(ARG_ON_VIEW_FINISHED);
        if (titleColor == NO_RESOURCE) {
            final int colorRes = arguments.getInt(ARG_TITLE_COLOR_RES, NO_RESOURCE);
            if (colorRes != NO_RESOURCE) titleColor = ContextCompat.getColor(getContext(), colorRes);
        }
        if (toolbarColor == NO_RESOURCE) {
            final int colorRes = arguments.getInt(ARG_TOOLBAR_COLOR_RES, NO_RESOURCE);
            if (colorRes != NO_RESOURCE) toolbarColor = ContextCompat.getColor(getContext(), colorRes);
        }
        if (backgroundColor == NO_RESOURCE) {
            final int colorRes = arguments.getInt(ARG_BACKGROUND_RES, NO_RESOURCE);
            if (colorRes != NO_RESOURCE) backgroundColor = ContextCompat.getColor(getContext(), colorRes);
        }
        if (backButtonColor == NO_RESOURCE) {
            final int colorRes = arguments.getInt(ARG_BACK_BUTTON_COLOR_RES, NO_RESOURCE);
            if (colorRes != NO_RESOURCE) backButtonColor = ContextCompat.getColor(getContext(), colorRes);
        }
        if (closeButtonIcon != NO_RESOURCE) backButtonColor = NO_RESOURCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.ilhasoft_core_fragment_media_view, container, false);
        medias = (ViewPager) view.findViewById(R.id.medias);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        configureToolbar(activity);

        if (backgroundColor != NO_RESOURCE) view.setBackgroundColor(backgroundColor);

        medias.setAdapter(new MediaViewAdapter(getChildFragmentManager(), mediaList));
        medias.setPageTransformer(true, new DepthPageTransformer());
        medias.addOnPageChangeListener(onPageChangeListener);
        medias.setCurrentItem(position);

        onPageChangeListener.onPageSelected(position);

        ActivityCompat.startPostponedEnterTransition(activity);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.ilhasoft_core_media_view, menu);
        mnOpen = menu.findItem(R.id.mn_open);
        onPageChangeListener.onPageSelected(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (onClickCloseListener != null) onClickCloseListener.onClick(this);
            return true;
        } else if (mnOpen != null && mnOpen.getItemId() == item.getItemId()) {
            final int currentItem = medias.getCurrentItem();
            final MediaModel media = mediaList.get(currentItem);
            if (currentItem < mediaList.size() && onClickMediaListener != null) {
                onClickMediaListener.onClick(this, media);
            }
            if (!isDefaultClickDisabled()) {
                media.openMedia(getContext());
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Nullable
    OnClickMediaListener getOnClickMediaListener() {
        return onClickMediaListener;
    }

    public boolean isDefaultClickDisabled() {
        return defaultClickDisabled;
    }

    @SuppressWarnings("ConstantConditions")
    private void configureToolbar(final AppCompatActivity activity) {
        final ActionBar actionBar;
        activity.setSupportActionBar(toolbar);
        actionBar = activity.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        activity.setTitle(null);

        if (titleColor != NO_RESOURCE) toolbar.setTitleTextColor(titleColor);
        if (toolbarColor != NO_RESOURCE) toolbar.setBackgroundColor(toolbarColor);

        if (closeButtonIcon != NO_RESOURCE) {
            actionBar.setHomeAsUpIndicator(closeButtonIcon);
        } else if (backButtonColor != NO_RESOURCE) {
            actionBar.setHomeAsUpIndicator(ActionBarHelper.getArrow(getContext(), backButtonColor));
        } else {
            final TypedValue typedValue = new TypedValue();
            TypedArray a = getContext().obtainStyledAttributes(typedValue.data, new int[] { android.R.attr.textColorPrimary });
            actionBar.setHomeAsUpIndicator(ActionBarHelper.getArrow(getContext(), a.getColor(0, 0xFFFFFF)));
            a.recycle();
        }
    }

    private void closeMediaViewFragmentDelayed() {
        medias.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (onViewFinishedListener != null) onViewFinishedListener.onViewFinished(MediaViewFragment.this);
            }
        }, 500);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
        @Override
        public void onPageSelected(int position) {
            final MediaModel media = (position < mediaList.size()) ? mediaList.get(position) : null;
            if (media != null) {
                if (titleEnabled) toolbar.setTitle(media.getTitle());
                if (mnOpen == null) return;

                boolean showOpenOption = media.hasOpenOption() && media.getOpenDrawable() != MediaModel.NO_RESOURCE;
                if (showOpenOption) {
                    mnOpen.setVisible(true);
                    mnOpen.setIcon(media.getOpenDrawable());
                } else {
                    mnOpen.setIcon(null);
                    mnOpen.setVisible(false);
                }
            } else {
                closeMediaViewFragmentDelayed();
            }
        }
        @Override
        public void onPageScrollStateChanged(int state) { }
    };

    private static final class MediaViewAdapter extends FragmentStatePagerAdapter {
        private final List<MediaModel> medias;
        public MediaViewAdapter(FragmentManager fragmentManager, List<MediaModel> medias) {
            super(fragmentManager);
            this.medias = medias;
        }
        @Override
        public Fragment getItem(int position) {
            if (position < getCount() - 1) {
                return MediaFragment.newInstance(medias.get(position));
            } else {
                return new Fragment();
            }
        }
        @Override
        public int getCount() {
            return medias.size() + 1;
        }
    }

    public static final class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);
            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);
            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

}
