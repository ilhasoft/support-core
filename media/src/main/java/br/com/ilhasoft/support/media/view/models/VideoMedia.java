package br.com.ilhasoft.support.media.view.models;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import androidx.annotation.DrawableRes;

import br.com.ilhasoft.support.media.R;

/**
 * A simple implementation for a video media. This implementation show a button and
 * open the video itself. This will be more than sufficient for most images.
 *
 * Created by john on 10/06/16.
 */
public class VideoMedia extends UriMedia {

    private String url;

    public VideoMedia(String url) {
        super(url);
    }

    public VideoMedia(String url, String thumbnailUri) {
        super(thumbnailUri);
        this.url = url;
    }

    protected VideoMedia(Parcel in) {
        super(in);
        this.url = in.readString();
    }

    @Override
    public int getTitle() {
        return R.string.video;
    }

    @DrawableRes
    @Override
    public int getOverlayDrawable() {
        return R.drawable.img_button_play;
    }

    @Override
    public boolean hasOpenOption() {
        return true;
    }

    @Override
    public int getOpenDrawable() {
        return R.drawable.ic_open_in_browser_white_24dp;
    }

    @Override
    public void openMedia(Context context) {
        Uri videoUri = Uri.parse(url);

        Intent intent = new Intent(Intent.ACTION_VIEW, videoUri);
        intent.setDataAndType(videoUri, "video/*");
        context.startActivity(intent);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.url);
    }

    public static final Creator<VideoMedia> CREATOR = new Creator<VideoMedia>() {
        @Override
        public VideoMedia createFromParcel(Parcel source) {
            return new VideoMedia(source);
        }
        @Override
        public VideoMedia[] newArray(int size) {
            return new VideoMedia[size];
        }
    };

}
