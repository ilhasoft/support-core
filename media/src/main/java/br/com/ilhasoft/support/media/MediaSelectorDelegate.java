package br.com.ilhasoft.support.media;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.github.rubensousa.bottomsheetbuilder.adapter.BottomSheetItemClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.ilhasoft.support.core.helpers.MediaIntentHelper;
import br.com.ilhasoft.support.core.helpers.PermissionHelper;

/**
 * This class handles the selection of images from camera or gallery, videos from camera and files.
 * AppCompatActivity result and permissions granting for Android 6.0 are handled in this class as well.
 * <p>
 * It's necessary to create a instance using the constructor {@link #MediaSelectorDelegate(Context, String)},
 * pass activity result to {@link #onActivityResult(int, int, Intent)}
 * and permissions results to {@link #onRequestPermissionResult(Fragment, int, int[])} or {@link #onRequestPermissionResult(AppCompatActivity, int, int[])}.
 */
public class MediaSelectorDelegate {

    private static final int DEFAULT_AUDIO_DURATION = 50 * 1000;

    private static final int REQUEST_PICK_FROM_GALLERY = 4501;
    private static final int REQUEST_IMAGE_CAPTURE = 4502;
    private static final int REQUEST_VIDEO_FROM_CAMERA = 4503;
    private static final int REQUEST_FILE = 4504;

    private static final int REQUEST_CODE_WRITE_EXTERNAL_IMAGE_PERMISSION = 201;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_GALLERY_PERMISSION = 202;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_VIDEO_PERMISSION = 203;
    private static final int REQUEST_CODE_READ_EXTERNAL_FILE_PERMISSION = 204;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_AUDIO_PERMISSION = 205;

    public static final int CAMERA_IMAGE = 0x01;
    public static final int GALLERY_IMAGE = 0x02;
    public static final int YOUTUBE = 0x04;
    public static final int VIDEO = 0x08;
    public static final int FILE = 0x10;
    public static final int AUDIO = 0x20;
    public static final int ALL = CAMERA_IMAGE | GALLERY_IMAGE | YOUTUBE | VIDEO | FILE | AUDIO;

    private Context context;
    private String providerAuthority;

    private File imageFromCamera;

    private OnLoadMediaListener onLoadImageListener;
    private OnLoadMultipleMediaListener onLoadGalleryMultipleImagesListener;
    private OnLoadMediaListener onLoadGalleryImageListener;
    private OnLoadMultipleMediaListener onLoadMultipleFilesListener;
    private OnLoadMediaListener onLoadFileListener;
    private OnLoadMediaListener onLoadVideoListener;
    private OnLoadAudioListener onLoadAudioListener;
    private OnErrorLoadingMediaListener onErrorLoadingMediaListener;
    private YoutubeHelper.OnPickYoutubeVideoListener onPickYoutubeVideoListener;

    private int picGalleryRequestCode = REQUEST_PICK_FROM_GALLERY;
    private int picCaptureRequestCode = REQUEST_IMAGE_CAPTURE;
    private int videoRequestCode = REQUEST_VIDEO_FROM_CAMERA;
    private int fileRequestCode = REQUEST_FILE;
    private int videoDuration = MediaIntentHelper.VIDEO_DURATION_LIMIT;
    private int audioDuration = DEFAULT_AUDIO_DURATION;
    private boolean multipleGalleryImages = false;
    private boolean multipleFiles = false;

    public MediaSelectorDelegate(Context context, String providerAuthority) {
        this.context = context;
        this.providerAuthority = providerAuthority;
    }

    public MediaSelectorDelegate selectMedia(final AppCompatActivity activity, int mediaTypes) {
        BottomSheetItemClickListener clickListener = getClickListenerForActivity(activity);
        buildBottomSheetForMedia(createMenu(mediaTypes), clickListener);
        return this;
    }

    private void createMenuItem(int mediaTypes, int checkType, MenuBuilder menuBuilder, @IdRes int id
            , @StringRes int title, @DrawableRes int icon) {
        if ((mediaTypes & checkType) == checkType) {
            menuBuilder.add(1, id, 0, title).setIcon(icon);
        }
    }

    public MediaSelectorDelegate selectMedia(final Fragment fragment, int mediaTypes) {
        BottomSheetItemClickListener clickListener = getClickListenerForFragment(fragment);
        buildBottomSheetForMedia(createMenu(mediaTypes), clickListener);
        return this;
    }

    public MediaSelectorDelegate selectMedia(final AppCompatActivity activity) {
        BottomSheetItemClickListener clickListener = getClickListenerForActivity(activity);
        buildBottomSheetForMedia(createMenu(ALL), clickListener);
        return this;
    }

    public MediaSelectorDelegate selectMedia(final Fragment fragment) {
        BottomSheetItemClickListener clickListener = getClickListenerForFragment(fragment);
        buildBottomSheetForMedia(createMenu(ALL), clickListener);
        return this;
    }

    @NonNull
    private Menu createMenu(int mediaTypes) {
        MenuBuilder menuBuilder = new MenuBuilder(context);
        createMenuItem(mediaTypes, FILE, menuBuilder, R.id.file, R.string.file, R.drawable.ic_folder_grey_24dp);
        createMenuItem(mediaTypes, YOUTUBE, menuBuilder, R.id.youtube, R.string.youtube, R.drawable.ic_ondemand_video_black_24dp);
        createMenuItem(mediaTypes, AUDIO, menuBuilder, R.id.audio, R.string.audio, R.drawable.ic_audio_grey_24dp);
        createMenuItem(mediaTypes, VIDEO, menuBuilder, R.id.video, R.string.video, R.drawable.ic_videocam_grey_24dp);
        createMenuItem(mediaTypes, GALLERY_IMAGE, menuBuilder, R.id.gallery, R.string.gallery, R.drawable.ic_gallery_grey_24dp);
        createMenuItem(mediaTypes, CAMERA_IMAGE, menuBuilder, R.id.camera, R.string.camera, R.drawable.ic_camera_alt_grey_24dp);
        return menuBuilder;
    }

    private void buildBottomSheetForMedia(Menu menu, BottomSheetItemClickListener clickListener) {
        if (menu.size() == 1) {
            clickListener.onBottomSheetItemClick(menu.getItem(0));
        } else {
            BottomSheetMenuDialog dialog = new BottomSheetBuilder(context)
                    .setMode(BottomSheetBuilder.MODE_LIST)
                    .setBackgroundColor(Color.WHITE)
                    .setMenu(menu)
                    .setItemClickListener(clickListener)
                    .createDialog();
            dialog.show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == AppCompatActivity.RESULT_OK) {
            switch(requestCode) {
                case REQUEST_VIDEO_FROM_CAMERA:
                    setResultVideo(data); break;
                case REQUEST_PICK_FROM_GALLERY:
                    setResultImageByGallery(data); break;
                case REQUEST_IMAGE_CAPTURE:
                    setResultImageByCamera(); break;
                case REQUEST_FILE:
                    setResultFile(data);
            }
        }
    }

    private void setResultVideo(Intent data) {
        Uri videoUri = data.getData();
        if (onLoadVideoListener != null)
            onLoadVideoListener.onLoadMedia(videoUri);
    }

    private void setResultFile(Intent data) {
        if (!multipleFiles) {
            onLoadFileListener.onLoadMedia(data.getData());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && data.getClipData() != null) {
            List<Uri> uriList = getFilesFromIntent(data);
            onLoadMultipleFilesListener.onLoadMultipleMedia(uriList);
        } else if (data.getData() != null) {
            Uri dataUri = data.getData();
            if (onLoadMultipleFilesListener != null) {
                onLoadMultipleFilesListener.onLoadMultipleMedia(Collections.singletonList(dataUri));
            }
        } else if (onErrorLoadingMediaListener != null) {
            onErrorLoadingMediaListener.onErrorLoadingMedia(new IllegalStateException("Files not loaded"),
                    R.string.error_message_file);
        }
    }

    private void setResultImageByCamera() {
        if(imageFromCamera != null) {
            if(onLoadImageListener != null)
                onLoadImageListener.onLoadMedia(Uri.fromFile(imageFromCamera));
        } else if (onErrorLoadingMediaListener != null) {
            onErrorLoadingMediaListener.onErrorLoadingMedia(new IllegalStateException("Image not loaded by the camera"),
                    R.string.error_message_capture_image);
        }
    }

    private void setResultImageByGallery(Intent data) {
        if (!multipleGalleryImages) {
            onLoadGalleryImageListener.onLoadMedia(data.getData());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && data.getClipData() != null) {
            List<Uri> picturesUri = getFilesFromIntent(data);
            onLoadGalleryMultipleImagesListener.onLoadMultipleMedia(picturesUri);
        } else if(data.getData() != null) {
            Uri pictureUri = data.getData();
            if(onLoadGalleryMultipleImagesListener != null)
                onLoadGalleryMultipleImagesListener.onLoadMultipleMedia(Collections.singletonList(pictureUri));
        } else if (onErrorLoadingMediaListener != null) {
            onErrorLoadingMediaListener.onErrorLoadingMedia(new IllegalStateException("Files not loaded"),
                    R.string.error_message_file);
        }
    }

    @NonNull
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private List<Uri> getFilesFromIntent(Intent data) {
        List<Uri> picturesUri = new ArrayList<>();
        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
            ClipData.Item item = data.getClipData().getItemAt(i);
            Uri pictureUri = item.getUri();
            if (pictureUri != null) picturesUri.add(pictureUri);
        }
        return picturesUri;
    }

    @NonNull
    private BottomSheetItemClickListener getClickListenerForActivity(final AppCompatActivity activity) {
        return new BottomSheetItemClickListener() {
            @Override
            public void onBottomSheetItemClick(MenuItem item) {
                if (R.id.gallery == item.getItemId()) {
                    pickFromGallery(activity);
                } else if (R.id.camera == item.getItemId()) {
                    pickFromCamera(activity);
                } else if (R.id.file == item.getItemId()) {
                    pickFile(activity);
                } else if (R.id.video == item.getItemId()) {
                    pickVideoFromCamera(activity, videoDuration);
                } else if (R.id.youtube == item.getItemId()) {
                    pickFromYoutube();
                } else if (R.id.audio == item.getItemId()) {
                    pickAudio(activity, audioDuration);
                }
            }
        };
    }

    @NonNull
    private BottomSheetItemClickListener getClickListenerForFragment(final Fragment fragment) {
        return new BottomSheetItemClickListener() {
            @Override
            public void onBottomSheetItemClick(MenuItem item) {
                if (R.id.gallery == item.getItemId()) {
                    pickFromGallery(fragment);
                } else if (R.id.camera == item.getItemId()) {
                    pickFromCamera(fragment);
                } else if (R.id.file == item.getItemId()) {
                    pickFile(fragment);
                } else if (R.id.video == item.getItemId()) {
                    pickVideoFromCamera(fragment, videoDuration);
                } else if (R.id.audio == item.getItemId()) {
                    pickAudio(fragment, audioDuration);
                }
            }
        };
    }

    public void onRequestPermissionResult(Fragment fragment, int requestCode, int [] grantResults) {
        if (grantResults.length > 0 && PermissionHelper.allPermissionsGranted(grantResults)) {
            switch (requestCode) {
                case REQUEST_CODE_WRITE_EXTERNAL_GALLERY_PERMISSION:
                    pickFromGallery(fragment); break;
                case REQUEST_CODE_WRITE_EXTERNAL_IMAGE_PERMISSION:
                    pickFromCamera(fragment); break;
                case REQUEST_CODE_READ_EXTERNAL_FILE_PERMISSION:
                    pickFile(fragment); break;
                case REQUEST_CODE_WRITE_EXTERNAL_VIDEO_PERMISSION:
                    pickVideoFromCamera(fragment, videoDuration); break;
                case REQUEST_CODE_WRITE_EXTERNAL_AUDIO_PERMISSION:
                    pickAudio(fragment, audioDuration);
            }
        } else if (onErrorLoadingMediaListener != null) {
            onErrorLoadingMediaListener.onErrorLoadingMedia(new IllegalStateException("Permission not given"),
                    R.string.error_message_permission_required);
        }
    }

    public void onRequestPermissionResult(AppCompatActivity activity, int requestCode, int [] grantResults) {
        if (grantResults.length > 0 && PermissionHelper.allPermissionsGranted(grantResults)) {
            switch (requestCode) {
                case REQUEST_CODE_WRITE_EXTERNAL_GALLERY_PERMISSION:
                    pickFromGallery(activity); break;
                case REQUEST_CODE_WRITE_EXTERNAL_IMAGE_PERMISSION:
                    pickFromCamera(activity); break;
                case REQUEST_CODE_READ_EXTERNAL_FILE_PERMISSION:
                    pickFile(activity); break;
                case REQUEST_CODE_WRITE_EXTERNAL_VIDEO_PERMISSION:
                    pickVideoFromCamera(activity, videoDuration); break;
                case REQUEST_CODE_WRITE_EXTERNAL_AUDIO_PERMISSION:
                    pickAudio(activity, audioDuration);
            }
        } else {
            if (onErrorLoadingMediaListener != null) {
                onErrorLoadingMediaListener.onErrorLoadingMedia(new IllegalStateException("Permission not given"),
                        R.string.error_message_permission_required);
            }
        }
    }

    private void pickFromGallery(Fragment fragment) {
        try {
            if (!PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
            && !PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                fragment.requestPermissions(
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_WRITE_EXTERNAL_GALLERY_PERMISSION
                );
            } else {
                MediaIntentHelper.startIntentImageFromGallery(fragment, picGalleryRequestCode, multipleGalleryImages);
            }
        } catch(Exception exception) {
            Log.e("MediaSelector", "onClick ", exception);
            if (onErrorLoadingMediaListener != null) {
                onErrorLoadingMediaListener.onErrorLoadingMedia(exception, R.string.error_message_camera_permission);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void pickFromGallery(AppCompatActivity activity) {
        try {
            if (!PermissionHelper.isPermissionGranted(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && !PermissionHelper.isPermissionGranted(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                activity.requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_GALLERY_PERMISSION);
            } else {
                MediaIntentHelper.startIntentImageFromGallery(activity, picGalleryRequestCode, multipleGalleryImages);
            }
        } catch(Exception exception) {
            if (onErrorLoadingMediaListener != null) {
                onErrorLoadingMediaListener.onErrorLoadingMedia(exception, R.string.error_message_camera_permission);
            }
            Log.e("MediaSelector", "onClick ", exception);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void pickFromCamera(Fragment fragment) {
        try {
            if (!PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
            || !PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
            || !PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.CAMERA)) {
                fragment.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                         Manifest.permission.CAMERA,
                                                         Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_WRITE_EXTERNAL_IMAGE_PERMISSION);
            } else {
                imageFromCamera = MediaIntentHelper.startIntentImageFromCamera(fragment, providerAuthority, picCaptureRequestCode);
            }
        } catch(Exception exception) {
            if (onErrorLoadingMediaListener != null) {
                onErrorLoadingMediaListener.onErrorLoadingMedia(exception, R.string.error_message_camera_permission);
            }
            Log.e("MediaSelector", "onClick ", exception);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void pickFromCamera(AppCompatActivity activity) {
        try {
            if (!PermissionHelper.isPermissionGranted(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            || !PermissionHelper.isPermissionGranted(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
            || !PermissionHelper.isPermissionGranted(activity, Manifest.permission.CAMERA)) {
                activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                         Manifest.permission.CAMERA,
                                                         Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_WRITE_EXTERNAL_IMAGE_PERMISSION);
            } else {
                imageFromCamera = MediaIntentHelper.startIntentImageFromCamera(activity, providerAuthority, picCaptureRequestCode);
            }
        } catch(Exception exception) {
            if (onErrorLoadingMediaListener != null) {
                onErrorLoadingMediaListener.onErrorLoadingMedia(exception, R.string.error_message_camera_permission);
            }
            Log.e("MediaSelector", "onClick ", exception);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void pickFile(Fragment fragment) {
        if (!PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            fragment.requestPermissions(new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                    REQUEST_CODE_READ_EXTERNAL_FILE_PERMISSION);
        } else {
            MediaIntentHelper.startIntentFile(fragment, fileRequestCode, multipleFiles);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void pickFile(AppCompatActivity activity) {
        if (!PermissionHelper.isPermissionGranted(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            activity.requestPermissions(new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                    REQUEST_CODE_READ_EXTERNAL_FILE_PERMISSION);
        } else {
            MediaIntentHelper.startIntentFile(activity, fileRequestCode, multipleFiles);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void pickAudio(Fragment fragment, int audioDuration) {
        this.audioDuration = audioDuration;
        if (!PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
        || !PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.RECORD_AUDIO)) {
            fragment.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO}, REQUEST_CODE_WRITE_EXTERNAL_AUDIO_PERMISSION);
        } else {
            RecordAudioFragment.newInstance(audioDuration)
                    .setOnLoadAudioListener(onLoadAudioListener)
                    .show(fragment.getFragmentManager(), "recordAudioFragment");
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void pickAudio(AppCompatActivity activity, int audioDuration) {
        this.audioDuration = audioDuration;
        if (!PermissionHelper.isPermissionGranted(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        || !PermissionHelper.isPermissionGranted(activity, Manifest.permission.RECORD_AUDIO)) {
            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO}, REQUEST_CODE_WRITE_EXTERNAL_AUDIO_PERMISSION);
        } else {
            RecordAudioFragment.newInstance(audioDuration)
                    .setOnLoadAudioListener(onLoadAudioListener)
                    .show(activity.getSupportFragmentManager(), "recordAudioFragment");
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void pickVideoFromCamera(Fragment fragment, int videoDuration) {
        this.videoDuration = videoDuration;
        if (!PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
        || !PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
        || !PermissionHelper.isPermissionGranted(fragment.getContext(), Manifest.permission.CAMERA)) {
            fragment.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_WRITE_EXTERNAL_VIDEO_PERMISSION);
        } else {
            MediaIntentHelper.startIntentVideoFromCamera(fragment, videoRequestCode, videoDuration);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void pickVideoFromCamera(AppCompatActivity activity, int videoDuration) {
        this.videoDuration = videoDuration;
        if (!PermissionHelper.isPermissionGranted(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        || !PermissionHelper.isPermissionGranted(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
        || !PermissionHelper.isPermissionGranted(activity, Manifest.permission.CAMERA)) {
            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_WRITE_EXTERNAL_VIDEO_PERMISSION);
        } else {
            MediaIntentHelper.startIntentVideoFromCamera(activity, videoRequestCode, videoDuration);
        }
    }

    public void pickFromYoutube() {
        YoutubeHelper.pickVideoFromInput(context, onPickYoutubeVideoListener);
    }

    public MediaSelectorDelegate setOnLoadImageListener(OnLoadMediaListener onLoadImageListener) {
        this.onLoadImageListener = onLoadImageListener;
        return this;
    }

    public MediaSelectorDelegate setOnLoadGalleryImageListener(OnLoadMediaListener onLoadGalleryImageListener) {
        this.onLoadGalleryImageListener = onLoadGalleryImageListener;
        return this;
    }

    public MediaSelectorDelegate setOnLoadGalleryMultipleImagesListener(OnLoadMultipleMediaListener onLoadGalleryMultipleImagesListener) {
        this.onLoadGalleryMultipleImagesListener = onLoadGalleryMultipleImagesListener;
        return this;
    }

    public MediaSelectorDelegate setOnLoadAudioListener(OnLoadAudioListener onLoadAudioListener) {
        this.onLoadAudioListener = onLoadAudioListener;
        return this;
    }

    public MediaSelectorDelegate setOnLoadFileListener(OnLoadMediaListener onLoadFileListener) {
        this.onLoadFileListener = onLoadFileListener;
        return this;
    }

    public MediaSelectorDelegate setOnLoadMultipleFilesListener(OnLoadMultipleMediaListener onLoadMultipleFilesListener) {
        this.onLoadMultipleFilesListener = onLoadMultipleFilesListener;
        return this;
    }

    public MediaSelectorDelegate setOnLoadVideoListener(OnLoadMediaListener onLoadVideoListener) {
        this.onLoadVideoListener = onLoadVideoListener;
        return this;
    }

    public MediaSelectorDelegate setOnPickYoutubeVideoListener(YoutubeHelper.OnPickYoutubeVideoListener onPickYoutubeVideoListener) {
        this.onPickYoutubeVideoListener = onPickYoutubeVideoListener;
        return this;
    }

    public MediaSelectorDelegate setOnErrorLoadingMediaListener(OnErrorLoadingMediaListener onErrorLoadingMediaListener) {
        this.onErrorLoadingMediaListener = onErrorLoadingMediaListener;
        return this;
    }

    public MediaSelectorDelegate setPicGalleryRequestCode(int picGalleryRequestCode) {
        this.picGalleryRequestCode = picGalleryRequestCode;
        return this;
    }

    public MediaSelectorDelegate setPicCaptureRequestCode(int picCaptureRequestCode) {
        this.picCaptureRequestCode = picCaptureRequestCode;
        return this;
    }

    public MediaSelectorDelegate setVideoRequestCode(int videoRequestCode) {
        this.videoRequestCode = videoRequestCode;
        return this;
    }

    public MediaSelectorDelegate setFileRequestCode(int fileRequestCode) {
        this.fileRequestCode = fileRequestCode;
        return this;
    }

    public MediaSelectorDelegate setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
        return this;
    }

    public MediaSelectorDelegate setAudioDuration(int audioDuration) {
        this.audioDuration = audioDuration;
        return this;
    }

    public MediaSelectorDelegate enableMultipleGalleryImages() {
        this.multipleGalleryImages = true;
        return this;
    }

    public MediaSelectorDelegate disableMultipleGalleryImages() {
        this.multipleGalleryImages = false;
        return this;
    }

    public MediaSelectorDelegate enableMultipleFiles() {
        this.multipleFiles = true;
        return this;
    }

    public MediaSelectorDelegate disableMultipleFiles() {
        this.multipleFiles = false;
        return this;
    }

    public interface OnLoadAudioListener {
        void onLoadAudio(Uri uri, int duration);
    }

    public interface OnLoadMediaListener {
        void onLoadMedia(Uri uri);
    }

    public interface OnLoadMultipleMediaListener {
        void onLoadMultipleMedia(List<Uri> uriList);
    }

    public interface OnErrorLoadingMediaListener {
        void onErrorLoadingMedia(Exception exception, @StringRes int errorMessageId);
    }

}
