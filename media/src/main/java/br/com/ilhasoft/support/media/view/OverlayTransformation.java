package br.com.ilhasoft.support.media.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.squareup.picasso.Transformation;

/**
 * Just an implementation of the {@link com.squareup.picasso.Transformation} interface
 * using {@link br.com.ilhasoft.support.graphics.transformations.OverlayBitmapTransformation}.
 *
 * Created by daniel on 14/05/16.
 */
public class OverlayTransformation implements Transformation {

    private final Bitmap overlay;

    public OverlayTransformation(Bitmap overlay) {
        this.overlay = overlay;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        final Bitmap output = source.copy(Bitmap.Config.ARGB_8888, true);
        new Canvas(output).drawBitmap(overlay, getPosition(output.getWidth(), overlay.getWidth()),
                                      getPosition(output.getHeight(), overlay.getHeight()), null);
        source.recycle();
        return output;
    }

    @Override
    public String key() {
        return "overlay";
    }

    private float getPosition(int length1, int length2) {
        return ((length1 / 2f) - (length2 / 2f));
    }

}
