package br.com.ilhasoft.support.media.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

/**
 * Created by daniel on 25/08/16.
 */
public final class MediaViewActivity extends AppCompatActivity {

    static final String EXTRA_AUTO_CLOSE_ENABLED = "auto_close_enabled";

    public static Intent createIntent(Context context, Bundle args) {
        return new Intent(context, MediaViewActivity.class).putExtras(args);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            final Bundle extras = getIntent().getExtras();
            if (extras != null) {
                wrapperOnClickIfNeeded(extras);
                wrapperOnViewFinished(extras);
                wrapperOnClickClose(extras);
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content, MediaViewFragment.newInstance(extras), null)
                    .commit();
        }
    }

    private void wrapperOnClickClose(Bundle extras) {
        final OnClickCloseListener l = (OnClickCloseListener) extras.getSerializable(MediaViewFragment.ARG_ON_CLICK_CLOSE);
        extras.putSerializable(MediaViewFragment.ARG_ON_CLICK_CLOSE, new OnClickCloseListenerWrapper(l));
    }

    private void wrapperOnViewFinished(Bundle extras) {
        final OnViewFinishedListener l = (OnViewFinishedListener) extras.getSerializable(MediaViewFragment.ARG_ON_VIEW_FINISHED);
        extras.putSerializable(MediaViewFragment.ARG_ON_VIEW_FINISHED, new OnViewFinishedListenerWrapper(l));
    }

    private void wrapperOnClickIfNeeded(Bundle extras) {
        if (extras.getBoolean(EXTRA_AUTO_CLOSE_ENABLED, true)) {
            final OnClickMediaListener listener = (OnClickMediaListener) extras.getSerializable(MediaViewFragment.ARG_ON_CLICK);
            extras.putSerializable(MediaViewFragment.ARG_ON_CLICK, new OnClickMediaListenerWrapper(listener));
        }
    }

    public static final class OnClickMediaListenerWrapper extends WrapperSerializable<OnClickMediaListener>
            implements OnClickMediaListener {
        public OnClickMediaListenerWrapper(@Nullable OnClickMediaListener onClickMediaListener) {
            super(onClickMediaListener);
        }
        @Override
        public void onClick(MediaViewFragment mediaViewFragment, MediaModel mediaModel) {
            if (!mediaModel.hasOpenOption()) mediaViewFragment.getActivity().finish();
            else if (serializable != null) serializable.onClick(mediaViewFragment, mediaModel);
        }
    }

    public static final class OnViewFinishedListenerWrapper extends WrapperSerializable<OnViewFinishedListener>
            implements OnViewFinishedListener {
        public OnViewFinishedListenerWrapper(@Nullable OnViewFinishedListener onViewFinishedListener) {
            super(onViewFinishedListener);
        }
        @Override
        public void onViewFinished(MediaViewFragment mediaViewFragment) {
            if (serializable != null) serializable.onViewFinished(mediaViewFragment);
            mediaViewFragment.getActivity().finish();
        }
    }

    public static final class OnClickCloseListenerWrapper extends WrapperSerializable<OnClickCloseListener>
            implements OnClickCloseListener {
        public OnClickCloseListenerWrapper(@Nullable OnClickCloseListener onClickCloseListener) {
            super(onClickCloseListener);
        }
        @Override
        public void onClick(MediaViewFragment mediaViewFragment) {
            if (serializable != null) serializable.onClick(mediaViewFragment);
            mediaViewFragment.getActivity().finish();
        }
    }

    public static abstract class WrapperSerializable<S extends Serializable> implements Serializable {
        @Nullable
        protected final S serializable;
        public WrapperSerializable(@Nullable S serializable) {
            this.serializable = serializable;
        }
    }

}
