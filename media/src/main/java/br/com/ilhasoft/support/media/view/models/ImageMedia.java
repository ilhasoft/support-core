package br.com.ilhasoft.support.media.view.models;

import android.os.Parcel;
import androidx.annotation.DrawableRes;

import br.com.ilhasoft.support.core.R;

/**
 * A simple implementation for a image media. This implementation don't show a button and
 * don't provides a open option. This will be more than sufficient for most images.
 *
 * Created by daniel on 10/06/16.
 */
public class ImageMedia extends UriMedia {

    public ImageMedia(String url) {
        super(url);
    }

    protected ImageMedia(Parcel in) {
        super(in);
    }

    @Override
    public int getTitle() {
        return R.string.image;
    }

    @DrawableRes
    @Override
    public int getOverlayDrawable() {
        return NO_RESOURCE;
    }

    @Override
    public boolean hasOpenOption() {
        return false;
    }

    @Override
    public int getOpenDrawable() {
        return NO_RESOURCE;
    }

    public static final Creator<ImageMedia> CREATOR = new Creator<ImageMedia>() {
        @Override
        public ImageMedia createFromParcel(Parcel source) {
            return new ImageMedia(source);
        }
        @Override
        public ImageMedia[] newArray(int size) {
            return new ImageMedia[size];
        }
    };

}
