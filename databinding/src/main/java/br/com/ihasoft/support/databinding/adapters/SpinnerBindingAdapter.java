package br.com.ihasoft.support.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;

import java.util.List;

import br.com.ilhasoft.support.databinding.R;

/**
 * Created by john-mac on 5/28/16.
 */
@SuppressWarnings({"unused", "unchecked"})
public final class SpinnerBindingAdapter {

    private SpinnerBindingAdapter() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @BindingAdapter({"items"})
    public static <T> void setItems(Spinner spinner, List<T> items) {
        if (items != null) {
            ArrayAdapter adapter = new ArrayAdapter<>(spinner.getContext(), android.R.layout.simple_list_item_1, items);
            spinner.setAdapter(adapter);

            addDefaultTextIfNeeded(spinner, adapter);
        }
    }

    @BindingAdapter({"items", "itemId", "layout"})
    public static <T> void setItems(Spinner spinner, final List<T> items, final int itemId, final int layout) {
        if (items != null) {
            BaseAdapter adapter = new BindingSpinnerAdapter<>(items, itemId, layout);
            spinner.setAdapter(adapter);
        }
    }

    private static void addDefaultTextIfNeeded(Spinner spinner, ArrayAdapter adapter) {
        Object defaultText = spinner.getTag(R.id.spinner_default_text);
        if (defaultText != null) adapter.insert(defaultText, 0);
    }

    @BindingAdapter({"defaultText"})
    public static void setDefaultText(Spinner spinner, String defaultText) {
        if (spinner.getTag(R.id.spinner_default_text) == null) {
            spinner.setTag(R.id.spinner_default_text, defaultText);
        }
    }

}
