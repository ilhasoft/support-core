package br.com.ilhasoft.support.graphics.drawable;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.DrawableCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * A Drawable that manages an list of other Drawables. These are drawn in list
 * order, so the element with the largest index will be drawn on top.
 * <p>
 * This is a class that can substitute for simple uses the class
 * {@link android.graphics.drawable.LayerDrawable} only supporting pedding mode stack.
 *
 * Created by daniel on 10/05/16.
 */
public class StackLayerDrawable extends Drawable implements Drawable.Callback {

    private final List<Drawable> drawableList;

    public StackLayerDrawable() {
        this(new ArrayList<Drawable>());
    }

    public StackLayerDrawable(Drawable drawable) {
        this(new ArrayList<Drawable>());
        addDrawableInternal(drawable);
    }

    public StackLayerDrawable(List<Drawable> drawableList) {
        this.drawableList = new ArrayList<>();
        setDrawableListInternal(drawableList);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw(@NonNull Canvas canvas) {
        for (Drawable drawable : drawableList) {
            drawable.draw(canvas);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onBoundsChange(Rect bounds) {
        for (Drawable drawable : drawableList) {
            drawable.setBounds(bounds);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFilterBitmap(boolean filter) {
        super.setFilterBitmap(filter);
        for (Drawable drawable : drawableList) {
            drawable.setFilterBitmap(filter);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAlpha(int alpha) {
        for (Drawable drawable : drawableList) {
            drawable.setAlpha(alpha);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        for (Drawable drawable : drawableList) {
            drawable.setColorFilter(colorFilter);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isStateful() {
        for (Drawable drawable : drawableList) {
            if (drawable.isStateful()) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setState(@NonNull final int[] stateSet) {
        boolean result = super.setState(stateSet);
        for (Drawable drawable : drawableList) {
            result = drawable.setState(stateSet) || result;
        }
        return result;
    }

    public void jumpToCurrentState() {
        for (Drawable drawable : drawableList) {
            DrawableCompat.jumpToCurrentState(drawable);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setVisible(boolean visible, boolean restart) {
        boolean result = super.setVisible(visible, restart);
        for (Drawable drawable : drawableList) {
            result = drawable.setVisible(visible, restart) || result;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("WrongConstant")
    @Override
    public int getOpacity() {
        return (drawableList.isEmpty() ? 1 : drawableList.get(0).getOpacity());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIntrinsicWidth() {
        int width = -1;
        for (Drawable drawable : drawableList) {
            if (drawable.getIntrinsicWidth() > width) {
                width = drawable.getIntrinsicWidth();
            }
        }
        return width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIntrinsicHeight() {
        int height = -1;
        for (Drawable drawable : drawableList) {
            if (drawable.getIntrinsicHeight() > height) {
                height = drawable.getIntrinsicHeight();
            }
        }
        return height;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMinimumWidth() {
        int width = -1;
        for (Drawable drawable : drawableList) {
            if (drawable.getMinimumWidth() > width) {
                width = drawable.getMinimumWidth();
            }
        }
        return width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMinimumHeight() {
        int height = -1;
        for (Drawable drawable : drawableList) {
            if (drawable.getMinimumHeight() > height) {
                height = drawable.getMinimumHeight();
            }
        }
        return height;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getPadding(@NonNull Rect padding) {
        return super.getPadding(padding);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onLevelChange(int level) {
        boolean result = super.onLevelChange(level);
        for (Drawable drawable : drawableList) {
            result = drawable.setLevel(level) || result;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAutoMirrored(boolean mirrored) {
        for (Drawable drawable : drawableList) {
            DrawableCompat.setAutoMirrored(drawable, mirrored);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public boolean isAutoMirrored() {
        return (drawableList.isEmpty() ? super.isAutoMirrored() : drawableList.get(0).isAutoMirrored());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTint(int tint) {
        for (Drawable drawable : drawableList) {
            DrawableCompat.setTint(drawable, tint);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTintList(ColorStateList tint) {
        for (Drawable drawable : drawableList) {
            DrawableCompat.setTintList(drawable, tint);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTintMode(@NonNull PorterDuff.Mode tintMode) {
        for (Drawable drawable : drawableList) {
            DrawableCompat.setTintMode(drawable, tintMode);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHotspot(float x, float y) {
        for (Drawable drawable : drawableList) {
            DrawableCompat.setHotspot(drawable, x, y);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHotspotBounds(int left, int top, int right, int bottom) {
        for (Drawable drawable : drawableList) {
            DrawableCompat.setHotspotBounds(drawable, left, top, right, bottom);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void invalidateDrawable(@NonNull Drawable who) {
        invalidateSelf();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void scheduleDrawable(@NonNull Drawable who, @NonNull Runnable what, long when) {
        scheduleSelf(what, when);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unscheduleDrawable(@NonNull Drawable who, @NonNull Runnable what) {
        unscheduleSelf(what);
    }

    public List<Drawable> getDrawableList() {
        return drawableList;
    }

    public void setDrawableList(List<Drawable> drawableList) {
        setDrawableListInternal(drawableList);
    }

    public void addDrawable(Drawable drawable) {
        addDrawableInternal(drawable);
    }

    public void addDrawable(int location, Drawable drawable) {
        addDrawableInternal(location, drawable);
    }

    public boolean removeDrawable(Drawable drawable) {
        return removeDrawableInternal(drawable);
    }

    @Nullable
    public Drawable removeDrawable(int index) {
        return removeDrawableInternal(index);
    }

    public int size() {
        return drawableList.size();
    }

    private void setDrawableListInternal(List<Drawable> drawables) {
        while (!drawableList.isEmpty()) {
            drawableList.remove(0).setCallback(null);
        }
        for (Drawable drawable : drawables) {
            addDrawableInternal(drawable);
        }
    }

    private void addDrawableInternal(Drawable drawable) {
        drawable.setCallback(this);
        drawableList.add(drawable);
    }

    private void addDrawableInternal(int location, Drawable drawable) {
        drawable.setCallback(this);
        drawableList.add(location, drawable);
    }

    private boolean removeDrawableInternal(Drawable drawable) {
        final boolean result = drawableList.remove(drawable);
        if (result) {
            drawable.setCallback(null);
        }
        return result;
    }

    @Nullable
    private Drawable removeDrawableInternal(int index) {
        final Drawable removed = drawableList.remove(index);
        if (removed != null) {
            removed.setCallback(null);
        }
        return removed;
    }

}
