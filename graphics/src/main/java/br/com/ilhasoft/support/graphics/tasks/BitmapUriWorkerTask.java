package br.com.ilhasoft.support.graphics.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import java.io.File;

import br.com.ilhasoft.support.core.helpers.IoHelper;
import br.com.ilhasoft.support.graphics.BitmapDecoder;
import br.com.ilhasoft.support.graphics.BitmapHelper;

/**
 * Created by johndalton on 12/07/14.
 */
public class BitmapUriWorkerTask extends BitmapWorkerTask<Uri> {

    private final Context context;

    public BitmapUriWorkerTask(Context context, ImageView imageView, int size) {
        super(imageView, size);
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(Uri... params) {
        final Uri uri = params[0];
        pathName = uri.getPath();

        final Bitmap bitmapDecoded = BitmapDecoder.decodeSampledBitmapFromUri(context, uri, size, size);
        try {
            return BitmapHelper.rotateBitmapIfNeeded(bitmapDecoded, new File(IoHelper.getFilePathForUri(context, uri)));
        } catch (Exception exception) {
            return bitmapDecoded;
        }
    }

}
