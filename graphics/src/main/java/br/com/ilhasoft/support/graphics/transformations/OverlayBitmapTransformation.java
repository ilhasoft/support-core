package br.com.ilhasoft.support.graphics.transformations;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by daniel on 14/05/16.
 */
public class OverlayBitmapTransformation implements BitmapTransformation {

    private final Bitmap overlay;

    public OverlayBitmapTransformation(Bitmap overlay) {
        this.overlay = overlay;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        final Bitmap output = source.copy(Bitmap.Config.ARGB_8888, true);
        new Canvas(output).drawBitmap(overlay, getPosition(output.getWidth(), overlay.getWidth()),
                                      getPosition(output.getHeight(), overlay.getHeight()), null);
        source.recycle();
        return output;
    }

    private float getPosition(int length1, int length2) {
        return ((length1 / 2f) - (length2 / 2f));
    }

}
