package br.com.ilhasoft.support.graphics.drawable.badge;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by daniel on 10/05/16.
 */
public class BadgeDrawable extends Drawable implements Badge {

    @Nullable
    private String badge;
    private Paint textPaint;
    private Paint badgePaint;
    private final Rect txtRect;

    /**
     * Simple constructor.
     */
    public BadgeDrawable() {
        this(Color.BLACK, Color.WHITE, 10);
    }

    /**
     * Simple constructor.
     *
     * @param badgeColor The badge color
     * @param badgeTextColor The badge text color
     * @param badgeTextSize The badge text size
     */
    public BadgeDrawable(@ColorInt int badgeColor, @ColorInt int badgeTextColor,
                         float badgeTextSize) {
        mutate();
        txtRect = new Rect();

        badgePaint = new Paint();
        badgePaint.setAntiAlias(true);
        badgePaint.setColor(badgeColor);
        badgePaint.setStyle(Paint.Style.FILL);

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(badgeTextColor);
        textPaint.setTextSize(badgeTextSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw(@NonNull Canvas canvas) {
        if (TextUtils.isEmpty(badge)) return;

        textPaint.getTextBounds(badge, 0, 1, txtRect);

        // Position the badge in the top-right quadrant of the icon.
        final Rect bounds = getBounds();
        final float radius = Math.abs(txtRect.top) * 1.1f;
        final float centerX = bounds.right - bounds.left - radius;

        // Draw badge circle.
        canvas.drawCircle(centerX, radius, radius, badgePaint);
        // Draw badge text inside the circle.
        canvas.drawText(badge, centerX, 1.5f * radius - (radius * 0.1f), textPaint);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAlpha(int alpha) { /* Do nothing */ }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) { /* Do nothing */ }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public String getBadge() {
        return badge;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadge(@Nullable String badge) {
        if (!TextUtils.equals(this.badge, badge)) {
            this.badge = badge;
            invalidateSelf();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeNumber(int badgeNumber) {
        if (badgeNumber < 0) {
            setBadge(Integer.toString(0));
        } else if (badgeNumber < 100) {
            setBadge(Integer.toString(badgeNumber));
        } else {
            setBadge(Integer.toString(99));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeColor(@ColorInt int badgeColor) {
        if (badgePaint.getColor() != badgeColor) {
            badgePaint.setColor(badgeColor);
            invalidateSelf();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeTextColor(@ColorInt int badgeTextColor) {
        if (textPaint.getColor() != badgeTextColor) {
            textPaint.setColor(badgeTextColor);
            invalidateSelf();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeTextSize(float badgeTextSize) {
        if (textPaint.getTextSize() != badgeTextSize) {
            textPaint.setTextSize(badgeTextSize);
            invalidateSelf();
        }
    }

}
