package br.com.ilhasoft.support.graphics.drawable.badge;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import android.view.MenuItem;

import java.util.List;

import br.com.ilhasoft.support.graphics.R;
import br.com.ilhasoft.support.graphics.drawable.StackLayerDrawable;

/**
 * Created by daniel on 10/05/16.
 */
public class BadgeOptionMenuDrawable extends StackLayerDrawable implements Badge {

    private final int padding;
    private final boolean needFixPadding;
    private final BadgeDrawable badgeDrawable;

    public static BadgeOptionMenuDrawable createWithIcon(Context context, MenuItem item, @DrawableRes int notificationIcon) {
        final Resources resources = context.getResources();
        return createWithDrawable(context, resources, item,
                ResourcesCompat.getDrawable(resources, notificationIcon, context.getTheme()),
                getDefaultPadding(context));
    }

    public static BadgeOptionMenuDrawable createWithIcon(Context context, MenuItem item, @DrawableRes int notificationIcon, int padding) {
        final Resources resources = context.getResources();
        return createWithDrawable(context, resources, item,
                ResourcesCompat.getDrawable(resources, notificationIcon, context.getTheme()), padding);
    }

    public static BadgeOptionMenuDrawable createWithDrawable(Context context, MenuItem item, Drawable drawable) {
        return createWithDrawable(context, context.getResources(), item, drawable, getDefaultPadding(context));
    }

    private static BadgeOptionMenuDrawable createWithDrawable(Context context, Resources resources, MenuItem item, Drawable drawable, int padding) {
        final BadgeOptionMenuDrawable optionMenuDrawable = new BadgeOptionMenuDrawable(context, drawable, padding);
        optionMenuDrawable.badgeDrawable.setBadgeTextSize(resources.getDimension(R.dimen.ilhasoft_badge_text_size));
        optionMenuDrawable.badgeDrawable.setBadgeColor(Color.RED);
        item.setIcon(optionMenuDrawable);
        return optionMenuDrawable;
    }

    private static int getDefaultPadding(Context context) {
        return Math.round(context.getResources().getDimension(R.dimen.ilhasoft_badge_padding));
    }

    /**
     * Simple constructor. The icon of the option menu is obtained through
     * {@link ContextCompat#getDrawable(Context, int)}.
     *
     * @param context The application's environment
     * @param drawableId The identifier of the drawable to use as the icon.
     */
    public BadgeOptionMenuDrawable(Context context, @DrawableRes int drawableId, int padding) {
        this(context, ContextCompat.getDrawable(context, drawableId), padding);
    }

    /**
     * Simple constructor.
     *
     * @param context The application's environment
     * @param drawableIcon The drawable icon to display on option menu
     */
    public BadgeOptionMenuDrawable(Context context, Drawable drawableIcon, int padding) {
        badgeDrawable = new BadgeDrawable();
        this.padding = padding;
        needFixPadding = Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;

        if (needFixPadding) super.addDrawable(ContextCompat.getDrawable(context, R.drawable.ilhasoft_ic_fix_badge));
        super.addDrawable(new InsetDrawable(drawableIcon, padding));
        super.addDrawable(badgeDrawable);
    }

    @Override
    public void setDrawableList(List<Drawable> drawableList) {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    @Override
    public void addDrawable(Drawable drawable) {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    @Override
    public void addDrawable(int location, Drawable drawable) {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    @Override
    public boolean removeDrawable(Drawable drawable) {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    @Override
    public Drawable removeDrawable(int index) {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public String getBadge() {
        return badgeDrawable.getBadge();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadge(@Nullable String badge) {
        badgeDrawable.setBadge(badge);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeNumber(int badgeNumber) {
        badgeDrawable.setBadgeNumber(badgeNumber);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeColor(@ColorInt int badgeColor) {
        badgeDrawable.setBadgeColor(badgeColor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeTextColor(@ColorInt int badgeTextColor) {
        badgeDrawable.setBadgeTextColor(badgeTextColor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBadgeTextSize(float badgeTextSize) {
        badgeDrawable.setBadgeTextSize(badgeTextSize);
    }

    public void setIconDrawable(Drawable drawableIcon) {
        final int position = needFixPadding ? 1 : 0;
        super.removeDrawable(position);
        super.addDrawable(position, new InsetDrawable(drawableIcon, padding));
        invalidateSelf();
    }

}
