package br.com.ilhasoft.support.core.app;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * Created by daniel on 23/05/16.
 */
public final class FragmentTransactionHelper {

    private FragmentTransactionHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");

    }

    public static void addFragment(FragmentActivity activity, @IdRes int containerViewId,
                                   Fragment fragment) {
        FragmentTransactionHelper.addFragment(activity.getSupportFragmentManager(),
                                              containerViewId, fragment, false);
    }

    public static void addFragment(FragmentManager fragmentManager, @IdRes int containerViewId,
                                   Fragment fragment) {
        FragmentTransactionHelper.addFragment(fragmentManager, containerViewId, fragment, false);
    }

    public static void addFragment(FragmentManager fragmentManager, @IdRes int containerViewId,
                                   Fragment fragment, boolean withBackStack) {
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (withBackStack) fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.add(containerViewId, fragment).commit();
    }

    public static void replaceFragment(FragmentActivity activity, @IdRes int containerViewId,
                                       Fragment fragment, boolean withBackStack) {
        FragmentTransactionHelper.replaceFragment(activity.getSupportFragmentManager(),
                                                  containerViewId, fragment, withBackStack);
    }

    public static void replaceFragment(FragmentManager fragmentManager, @IdRes int containerViewId,
                                       Fragment fragment, boolean withBackStack) {
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (withBackStack) fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(containerViewId, fragment).commit();
    }

}
