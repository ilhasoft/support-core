package br.com.ilhasoft.support.core.helpers;

import android.app.Activity;
import android.content.Context;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Helper class to show or hide the virtual keyboard
 *
 * Created by daniel on 22/05/16.
 */
public final class KeyboardHelper {

    private KeyboardHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static void showForced(Activity activity) {
        KeyboardHelper.changeVisibilityInternal(activity.getCurrentFocus(), InputMethodManager.SHOW_FORCED);
    }

    public static void showForced(Fragment fragment) {
        KeyboardHelper.changeVisibilityInternal(fragment.getView(), InputMethodManager.SHOW_FORCED);
    }

    public static void showForced(View view) {
        KeyboardHelper.changeVisibilityInternal(view, InputMethodManager.SHOW_FORCED);
    }

    public static void hideForced(Activity activity) {
        KeyboardHelper.changeVisibilityInternal(activity.getCurrentFocus(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void hideForced(Fragment fragment) {
        KeyboardHelper.changeVisibilityInternal(fragment.getView(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void hideForced(View view) {
        KeyboardHelper.changeVisibilityInternal(view, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private static void changeVisibilityInternal(View view, int flags) {
        ((InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(view.getWindowToken(), flags);
    }

}
