package br.com.ilhasoft.support.core.mvp;

import androidx.annotation.StringRes;

/**
 * Created by daniel on 04/05/16.
 */
public interface BasicView {

    void showMessage(@StringRes int messageId);

    void showMessage(CharSequence message);

    void showLoading();

    void dismissLoading();

}
