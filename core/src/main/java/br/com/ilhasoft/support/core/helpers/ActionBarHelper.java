package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;

/**
 * Created by daniel on 25/05/16.
 */
public final class ActionBarHelper {

    private ActionBarHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static Drawable getArrowRes(Context context, @ColorRes int colorRes) {
        return ActionBarHelper.getArrow(context, ContextCompat.getColor(context, colorRes));
    }

    public static Drawable getArrow(Context context, @ColorInt int color) {
        final DrawerArrowDrawable drawable = new DrawerArrowDrawable(context);
        drawable.setProgress(1.0f);
        drawable.setColor(color);
        return drawable;
    }

    public static Drawable getSandwichRes(Context context, @ColorRes int colorRes) {
        return ActionBarHelper.getSandwich(context, ContextCompat.getColor(context, colorRes));
    }

    public static Drawable getSandwich(Context context, @ColorInt int color) {
        final DrawerArrowDrawable drawable = new DrawerArrowDrawable(context);
        drawable.setProgress(0.0f);
        drawable.setColor(color);
        return drawable;
    }

}
