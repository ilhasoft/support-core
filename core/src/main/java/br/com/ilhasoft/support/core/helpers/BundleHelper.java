package br.com.ilhasoft.support.core.helpers;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by daniel on 13/05/16.
 */
public final class BundleHelper {

    private BundleHelper() {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    public static Bundle ensureExtras(Activity activity, String...extras) {
        return BundleHelper.ensure(activity.getIntent().getExtras(),
                                   activity + " must be started using the intent created by createIntent method.",
                                   extras);
    }

    public static Bundle ensureArguments(Fragment fragment, String...extras) {
        return BundleHelper.ensure(fragment.getArguments(),
                                   fragment + " must be instantiate by calling newInstance method.",
                                   extras);
    }

    public static Bundle ensureArguments(android.app.Fragment fragment, String...extras) {
        return BundleHelper.ensure(fragment.getArguments(),
                                   fragment + " must be instantiate by calling newInstance method.",
                                   extras);
    }

    public static Bundle ensure(@Nullable final Bundle bundle,
                                final String errorMessage, final String...keys) {
        if (bundle == null) throw new IllegalArgumentException(errorMessage);

        for (String key : keys) {
            if (!bundle.containsKey(key)) throw new RuntimeException(errorMessage);
        }

        return bundle;
    }

}
