package br.com.ilhasoft.support.core.animation;

import android.os.Handler;

/**
 * Created by daniel on 29/11/15.
 */
public class PowerSaveProgressAnimator extends ProgressAnimator {

    private final int steps;
    private int currentStep = 0;
    private final Handler handler;
    private boolean isRunning = false;

    public PowerSaveProgressAnimator(final int steps, final ProgressAnimatorListener progressAnimatorListener) {
        super(progressAnimatorListener);
        handler = new Handler();
        if (steps > 0 && steps <= 10)
            this.steps = steps;
        else
            this.steps = 5;
    }

    @Override
    public void setDuration(int duration) {
        if (!isRunning)
            super.setDuration(duration);
    }

    @Override
    public void start() {
        if (!isRunning) {
            isRunning = true;
            handler.postDelayed(runnable, duration / steps);
            if (progressAnimatorListener != null)
                progressAnimatorListener.onAnimationUpdate(100);
        }
    }

    @Override
    public void stop() {
        if (isRunning) {
            isRunning = false;
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentStep < steps) {
                ++currentStep;
                if (progressAnimatorListener != null) {
                    progressAnimatorListener.onAnimationUpdate(100 - 100 * currentStep / steps);
                    if (currentStep == steps) {
                        isRunning = false;
                        progressAnimatorListener.onAnimationEnd();
                    } else
                        handler.postDelayed(runnable, duration / steps);
                }
            }
        }
    };

}
