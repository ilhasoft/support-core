package br.com.ilhasoft.support.core.animation;

/**
 * Created by daniel on 29/11/15.
 */
public abstract class ProgressAnimator {

    protected int duration = 600;
    protected final ProgressAnimatorListener progressAnimatorListener;

    public ProgressAnimator(final ProgressAnimatorListener progressAnimatorListener) {
        this.progressAnimatorListener = progressAnimatorListener;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public abstract void start();

    public abstract void stop();

    public abstract boolean isRunning();

    public interface ProgressAnimatorListener {
        void onAnimationUpdate(float progress);
        void onAnimationEnd();
    }

}
