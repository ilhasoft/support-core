package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

/**
 * Created by john-mac on 5/25/16.
 */
public final class ShareHelper {

    private ShareHelper() {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    public static void shareBinary(Context context, File file, String type, String title) {
        Intent sharingIntent = new Intent();
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        sharingIntent.setType(type);
        context.startActivity(Intent.createChooser(sharingIntent, title));
    }

    public static void shareContent(Context context, String title, String subject, String text) {
        Intent sharingIntent = new Intent();
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sharingIntent, title));
    }

}
