package br.com.ilhasoft.support.core.animation;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.os.Build;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import br.com.ilhasoft.support.core.R;

/**
 * Created by johndalton on 22/08/14.
 */
public final class MediaAlphaAnimator {

    private MediaAlphaAnimator () {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    public static void showMedia(ImageView imageView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            final Animator alphaAnimator = AnimatorInflater.loadAnimator(imageView.getContext(),
                                                                         R.animator.media_in);
            alphaAnimator.setTarget(imageView);
            alphaAnimator.start();
        } else {
            final AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
            alphaAnimation.setDuration(100);
            imageView.startAnimation(alphaAnimation);
        }
    }

}
