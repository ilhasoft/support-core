package br.com.ilhasoft.support.core.helpers;

import java.util.concurrent.TimeUnit;

/**
 * Created by john-mac on 12/2/16.
 */
public final class TimeFormatHelper {

    private TimeFormatHelper() {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    public static String getTimeFormattedFromMillis(long milliseconds) {
        return getTimeFormattedFromSeconds(TimeUnit.MILLISECONDS.toSeconds(milliseconds));
    }

    public static String getTimeFormattedFromSeconds(long seconds) {
        return twoDigitString(TimeUnit.SECONDS.toMinutes(seconds)) + ":" + twoDigitString(seconds % 60);
    }

    private static String twoDigitString(long number) {
        if (number == 0) return "00";
        else if (number / 10 == 0) return "0" + number;
        else return String.valueOf(number);
    }

}
