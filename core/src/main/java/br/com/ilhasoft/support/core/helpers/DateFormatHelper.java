package br.com.ilhasoft.support.core.helpers;

import android.text.format.DateUtils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by johncordeiro on 14/08/15.
 */
public final class DateFormatHelper {

    private DateFormatHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static String getTimeElapsed(final Date timeAgo, final String nowTime) {
        return DateFormatHelper.getTimeElapsed(timeAgo.getTime(), nowTime);
    }

    public static String getTimeElapsed(final long timeAgo, final String nowTime) {
        final long currentTime = System.currentTimeMillis();
        final long timeElapsedInMillis = currentTime - timeAgo;
        final int seconds = (int) Math.abs(timeElapsedInMillis / 1000);
        final int minutes = seconds / 60;
        final int hours = minutes / 60;
        final int days = hours / 24;

        final String time;
        if (days >= 1) {
            time = DateUtils.getRelativeTimeSpanString(currentTime - TimeUnit.DAYS.toMillis(days)).toString();
        } else if (hours >= 1) {
            time = DateUtils.getRelativeTimeSpanString(currentTime - TimeUnit.HOURS.toMillis(hours)).toString();
        } else if (minutes >= 1) {
            time = DateUtils.getRelativeTimeSpanString(currentTime - TimeUnit.MINUTES.toMillis(minutes)).toString();
        } else {
            time = nowTime;
        }
        return time;
    }

}
