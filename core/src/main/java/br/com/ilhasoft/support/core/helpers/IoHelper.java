package br.com.ilhasoft.support.core.helpers;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by johncordeiro on 20/08/15.
 */
public final class IoHelper {

    public static final String EXTERNAL_STORAGE_AUTHORITY = "com.android.externalstorage.documents";
    public static final String DOWNLOAD_DOCUMENTS_AUTHORITY = "com.android.providers.downloads.documents";
    public static final String MEDIA_DOCUMENT_AUTHORITY = "com.android.providers.media.documents";
    public static final String DOWNLOAD_DOCUMENTS_URI_PREFIX = "content://downloads/public_downloads";

    private static final String FORMAT_TIMESTAMP = "yyyyMMdd-HHmmss";

    private IoHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @Nullable
    public static String getMimeTypeFromURL(String url) {
        final String extension = fileExt(url);
        final MimeTypeMap mimeType = MimeTypeMap.getSingleton();
        return TextUtils.isEmpty(extension) ? null : mimeType.getMimeTypeFromExtension(extension.substring(1));
    }

    @Nullable
    public static String getMimeTypeFromUri(Context context, Uri uri) {
        String type = context.getContentResolver().getType(uri);
        return TextUtils.isEmpty(type) ? null : MimeTypeMap.getSingleton().getMimeTypeFromExtension(type);
    }

    public static float getFileSizeInMb(File file) {
        final float fileSizeInBytes = file.length();
        final float fileSizeInKB = fileSizeInBytes / 1024;
        return fileSizeInKB / 1024;
    }

    public static File createImageFilePath(Context context) throws IOException {
        final File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final String imageFileName = "JPEG_" + getTimeStamp() + "_";
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public static File createImageFilePathPublic() throws IOException {
        final File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        final String imageFileName = "JPEG_" + getTimeStamp() + "_";
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public static File createVideoFilePath() throws IOException {
        final File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        final String imageFileName = "VIDEO_" + getTimeStamp() + "_";
        return File.createTempFile(imageFileName, ".mp4", storageDir);
    }

    public static File createAudioFilePath(String extension) throws IOException {
        final File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        final String audioFileName = "AUDIO_" + getTimeStamp() + "_";
        return File.createTempFile(audioFileName, extension, storageDir);
    }

    @SuppressLint("NewApi")
    public static String getFilePathForUri(Context context, Uri uri) throws URISyntaxException {
        boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;

        if (needToCheckUri && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                return getPathForExternalStorageDocument(uri);
            } else if (isDownloadsDocument(uri)) {
                uri = getUriForDownloadsDocument(uri);
            } else if (isMediaDocument(uri)) {
                final String documentId = DocumentsContract.getDocumentId(uri);
                final String[] split = documentId.split(":");
                final String type = split[0];
                uri = getUriByType(uri, type);
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        return getPathByProviderQuery(context, uri, selection, selectionArgs);
    }

    private static Uri getUriByType(Uri uri, String type) {
        switch (type) {
            case "image":
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                break;
            case "video":
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                break;
            case "audio":
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                break;
        }
        return uri;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static Uri getUriForDownloadsDocument(Uri uri) {
        final String id = DocumentsContract.getDocumentId(uri);
        return ContentUris.withAppendedId(Uri.parse(DOWNLOAD_DOCUMENTS_URI_PREFIX), Long.valueOf(id));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static String getPathForExternalStorageDocument(Uri uri) {
        final String documentId = DocumentsContract.getDocumentId(uri);
        final String[] split = documentId.split(":");
        return Environment.getExternalStorageDirectory() + "/" + split[1];
    }

    @Nullable
    private static String getPathByProviderQuery(Context context, Uri uri, String selection, String[] selectionArgs) {
        if (uri.getScheme().equalsIgnoreCase("file")) {
            return uri.getPath();
        } else if (uri.getScheme().equalsIgnoreCase("content")) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int dataIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst())
                    return cursor.getString(dataIndex);
            } catch (Exception ignored) {
                return null;
            } finally {
                if (cursor != null) cursor.close();
            }
        }
        return null;
    }

    @Nullable
    private static String fileExt(String url) {
        if (url.contains("?"))
            url = url.substring(0, url.indexOf("?"));
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf("."));
            if (ext.contains("%"))
                ext = ext.substring(0, ext.indexOf("%"));
            if (ext.contains("/"))
                ext = ext.substring(0, ext.indexOf("/"));
            return ext.toLowerCase();
        }
    }

    private static String getTimeStamp() {
        return new SimpleDateFormat(FORMAT_TIMESTAMP, Locale.US).format(new Date());
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return uri.getAuthority().equals(EXTERNAL_STORAGE_AUTHORITY);
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return uri.getAuthority().equals(DOWNLOAD_DOCUMENTS_AUTHORITY);
    }

    private static boolean isMediaDocument(Uri uri) {
        return uri.getAuthority().equals(MEDIA_DOCUMENT_AUTHORITY);
    }

}
