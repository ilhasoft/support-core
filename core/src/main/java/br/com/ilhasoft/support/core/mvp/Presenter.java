package br.com.ilhasoft.support.core.mvp;

import android.content.Intent;
import androidx.annotation.CallSuper;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by daniel on 21/03/16.
 */
public abstract class Presenter<View> {

    private final View viewNull;
    private WeakReference<View> viewReference;

    @SuppressWarnings("unchecked")
    public Presenter(final Class<View> viewClass) {
        if (!viewClass.isInterface()) {
            throw new IllegalArgumentException(String.format("View (%s) declaration must be a interface.", viewClass.getSimpleName()));
        }

        viewNull = (View) Proxy.newProxyInstance(viewClass.getClassLoader(), new Class<?>[] { viewClass }, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if ("equals".equals(method.getName())) {
                  return method.invoke(this, args);
                } else {
                    new IllegalStateException(String.format("There is no view (%s) attached to the run method: %s", viewClass.getSimpleName(), method.getName())).printStackTrace();
                    return null;
                }
            }
        });
        viewReference = new WeakReference<>(viewNull);
    }

    public final View getView() {
        View view = viewReference.get();
        if (view == null) {
            view = viewNull;
            this.attachInternal(viewNull);
        }
        return view;
    }

    public final boolean isAttached() {
        return !viewNull.equals(viewReference.get());
    }

    @CallSuper
    public void attachView(View view) {
        this.attachInternal(view);
    }

    public void start() { }

    public void onActivityResult(int requestCode, int resultCode, Intent data) { }

    public void stop() { }

    @CallSuper
    public void detachView() {
        this.attachInternal(viewNull);
    }

    private void attachInternal(View view) {
        viewReference.clear();
        viewReference = new WeakReference<>(view);
    }

}