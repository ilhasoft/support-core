package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import android.text.TextUtils;
import android.util.TypedValue;

/**
 * Created by john-mac on 5/25/16.
 */
public final class ResourceHelper {

    private ResourceHelper() {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    public static int getColorByAttr(Context context, int attrResource) {
        TypedValue typedValue = new TypedValue();

        TypedArray array = context.obtainStyledAttributes(typedValue.data, new int[] { attrResource });
        int color = array.getColor(0, 0);
        array.recycle();

        return color;
    }

    @DrawableRes
    public static int getDrawableId(Context context, String drawable, @DrawableRes int defaultRes) {
        if (!TextUtils.isEmpty(drawable)) {
            return context.getResources().getIdentifier(drawable, "drawable", context.getPackageName());
        } else {
            return defaultRes;
        }
    }

    @Nullable
    public static CharSequence getText(Context context, @StringRes int textId) {
        return (textId != 0) ? context.getText(textId) : null;
    }

    @Nullable
    public static String getString(Context context, @StringRes int textId) {
        return (textId != 0) ? context.getString(textId) : null;
    }

}
