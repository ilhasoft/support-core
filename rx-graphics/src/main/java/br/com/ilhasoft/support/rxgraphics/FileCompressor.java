package br.com.ilhasoft.support.rxgraphics;

import android.os.Environment;

import com.lalongooo.videocompressor.video.MediaController;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Callable;

import br.com.ilhasoft.support.graphics.BitmapCompressor;
import io.reactivex.Flowable;

/**
 * Created by john-mac on 9/23/16.
 */
public final class FileCompressor {

    private FileCompressor() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    /**
     * {@link #compressPicture(File, int, int, int)}
     */
    public static Flowable<File> compressPicture(final File pictureFile, final int jpegQuality) {
        return Flowable.fromCallable(new Callable<File>() {
            @Override
            public File call() throws IOException {
                return BitmapCompressor.compressFile(pictureFile, jpegQuality);
            }
        });
    }

    /**
     * {@link #compressPicture(File, int, int, int)}
     */
    public static Flowable<File> compressPicture(final File pictureFile, final int maxWidth, final int maxHeight) {
        return Flowable.fromCallable(new Callable<File>() {
            @Override
            public File call() throws IOException {
                return BitmapCompressor.compressFile(pictureFile, maxWidth, maxHeight);
            }
        });
    }

    /**
     * Compress picture file
     * @param pictureFile file to be compressed
     * @param maxWidth maximum width when scaling picture file
     * @param maxHeight maximum height when scaling picture file
     * @param jpegQuality quality of jpeg generated from compression
     * @return file compressed
     */
    public static Flowable<File> compressPicture(final File pictureFile, final int maxWidth,
                                                   final int maxHeight, final int jpegQuality) {
        return Flowable.fromCallable(new Callable<File>() {
            @Override
            public File call() throws IOException {
                return BitmapCompressor.compressFile(pictureFile, maxWidth, maxHeight, jpegQuality);
            }
        });
    }

    /**
     * Compress video file using telegram library
     * @return file compressed
     */
    public static Flowable<File> compressVideo(final File videoFile) {
        return Flowable.fromCallable(new Callable<File>() {
            @Override
            public File call() throws Exception {
                final File compressedFile = createDcimFilePath();
                final boolean converted = MediaController.getInstance().convertVideo(videoFile.getAbsolutePath(),
                                                                                     compressedFile.getAbsolutePath());
                if (converted) {
                    return compressedFile;
                } else {
                    throw new IllegalStateException("It was not possible to compress the video");
                }
            }
        });
    }

    private static File createDcimFilePath() throws IOException {
        final String timeStamp = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(new Date());
        final String imageFileName = "VIDEO_" + timeStamp + "_";
        final File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        return File.createTempFile(imageFileName, ".mp4", storageDir);
    }

}
